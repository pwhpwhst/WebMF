package action;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import MFCode.MFCode;
import Solver.MFSolver;
import Solver.MFSubStatusBuilder;
import Solver.MFTwoFaceSolver2;
import Solver.MFTwoSubStatusSolver;
import Solver.MFWriteFaceSolver;
import pojo.MFSubStatusList;

public class CubeAction {
	public CubeAction(){
//		private Map<String,String> cubeOrderToMFOrder;
//		private Map<String,String> mFOrderToCubeOrder;
		cubeOrderToMFOrder=new HashMap<String,String>();
		mFOrderToCubeOrder=new HashMap<String,String>();
		
		cubeOrderToMFOrder.put("F", "0");mFOrderToCubeOrder.put("0", "F");
		cubeOrderToMFOrder.put("f", "1");mFOrderToCubeOrder.put("1", "f");
										 mFOrderToCubeOrder.put("2", "F,F");
		cubeOrderToMFOrder.put("B", "3");mFOrderToCubeOrder.put("3", "B");
		cubeOrderToMFOrder.put("b", "4");mFOrderToCubeOrder.put("4", "b");
										 mFOrderToCubeOrder.put("5", "B,B");
		cubeOrderToMFOrder.put("R", "6");mFOrderToCubeOrder.put("6", "R");
		cubeOrderToMFOrder.put("r", "7");mFOrderToCubeOrder.put("7", "r");
										 mFOrderToCubeOrder.put("8", "R,R");
		cubeOrderToMFOrder.put("L", "9");mFOrderToCubeOrder.put("9", "L");
		cubeOrderToMFOrder.put("l", "10");mFOrderToCubeOrder.put("10", "l");
										  mFOrderToCubeOrder.put("11", "L,L");
		cubeOrderToMFOrder.put("U", "12");mFOrderToCubeOrder.put("12", "U");
		cubeOrderToMFOrder.put("u", "13");mFOrderToCubeOrder.put("13", "u");
										  mFOrderToCubeOrder.put("14", "U,U");
		cubeOrderToMFOrder.put("D", "15");mFOrderToCubeOrder.put("15", "D");
		cubeOrderToMFOrder.put("d", "16");mFOrderToCubeOrder.put("16", "d");
										  mFOrderToCubeOrder.put("17", "D,D");
		cubeOrderToMFOrder.put("\n", "\n");mFOrderToCubeOrder.put("\n", "\n");
										  
	}
	public MFCode getMf() {
		return mf;
	}

	public void setMf(MFCode mf) {
		this.mf = mf;
	}
	public MFTwoFaceSolver2 getmFTwoFaceSolver2() {
		return mFTwoFaceSolver2;
	}
	public void setmFTwoFaceSolver2(MFTwoFaceSolver2 mFTwoFaceSolver2) {
		this.mFTwoFaceSolver2 = mFTwoFaceSolver2;
	}
	private MFSubStatusList mFSubStatusList;
	private MFSubStatusBuilder mFSubStatusBuilder;
	public MFSolver getmFSolver() {
		return mFSolver;
	}
	public void setmFSolver(MFSolver mFSolver) {
		this.mFSolver = mFSolver;
	}
	private MFCode mf;
	private MFWriteFaceSolver mFWriteFaceSolver;
	private MFTwoFaceSolver2 mFTwoFaceSolver2;
	private MFSolver mFSolver;

	private String colsForCube;
	private String historyOrder;
	private Map<String,String> cubeOrderToMFOrder;
	private Map<String,String> mFOrderToCubeOrder;
	public Map<String, String> getDataMap() {
		return dataMap;
	}
	private Map<String,String> dataMap;
	public void setColsForCube(String colsForCube) {
		this.colsForCube = colsForCube;
	}
	public void setHistoryOrder(String historyOrder) {
		this.historyOrder = historyOrder;
	}

	public MFWriteFaceSolver getmFWriteFaceSolver() {
		return mFWriteFaceSolver;
	}
	public void setmFWriteFaceSolver(MFWriteFaceSolver mFWriteFaceSolver) {
		this.mFWriteFaceSolver = mFWriteFaceSolver;
	}
	public MFTwoSubStatusSolver getmFTwoSubStatusSolver() {
		return mFTwoSubStatusSolver;
	}
	public void setmFTwoSubStatusSolver(MFTwoSubStatusSolver mFTwoSubStatusSolver) {
		this.mFTwoSubStatusSolver = mFTwoSubStatusSolver;
	}
	private MFTwoSubStatusSolver mFTwoSubStatusSolver;
	public MFSubStatusBuilder getmFSubStatusBuilder() {
		return mFSubStatusBuilder;
	}

	public void setmFSubStatusBuilder(MFSubStatusBuilder mFSubStatusBuilder) {
		this.mFSubStatusBuilder = mFSubStatusBuilder;
	}

	
	public MFSubStatusList getmFSubStatusList() {
		return mFSubStatusList;
	}

	public void setmFSubStatusList(MFSubStatusList mFSubStatusList) {
		this.mFSubStatusList = mFSubStatusList;
		System.out.println("a");
	}
	
	public String test(){
		return "test";
	}
	
	public String test2(){
		return "test2";
	}
	
	public String test3(){
		int a=mFSubStatusList.getScore(84348175, false);
		System.out.println(a);
		return "test3";
	}
	
	public String test4(){
		String[] cols={
				"bowgworww",
				"owyoyywry",
				"ggybgwgrr",
				"bywbbroyg",
				"rrborwogr", 
				"ybogoygbb",	
		};
		mf.parseMFArray(cols);
		mf.showMF();
		
		mFSubStatusBuilder.builder(286528284, mf, false);
		mf.showMF();
		return "test4";
	}
	
	public String test5(){
		String[] cols={
				"bowgworww",
				"owyoyywry",
				"ggybgwgrr",
				"bywbbroyg",
				"rrborwogr", 
				"ybogoygbb",	
		};
		mf.parseMFArray(cols);
		mf.showMF();
//		mFTwoSubStatusSolver.initTwoMFs(1292175187,353836829);
		mFTwoSubStatusSolver.initTwoMFs(mf.extractMFSubStatus("00"),mf.extractMFSubStatus("01"));
		String str=mFTwoSubStatusSolver.solve();
//		9,12,6,0,12,4,12,15,4,12,17,4,15,11,8,13,8,11
		mf.move(str);
		mf.showMF();
		System.out.println("s");
		return "test5";
	}
	
	public String test6(){
		String[] cols={
				"bowgworww",
				"owyoyywry",
				"ggybgwgrr",
				"bywbbroyg",
				"rrborwogr", 
				"ybogoygbb",	
		};
		mf.parseMFArray(cols);
		mf.showMF();
		
		//mFWriteFaceSolver 白 On
		mFWriteFaceSolver.setInvert(false);
		mFWriteFaceSolver.setDetail(true);
		mFWriteFaceSolver.setOriginalPosition(mf.extractMFSubStatus("00", false),mf.extractMFSubStatus("01", false));
		String[] outStr={""};
		if(mFWriteFaceSolver.solver(true)!=-1){
			mFWriteFaceSolver.arrangeStep(outStr);
			System.out.println(outStr[0]);
		}
//mFWriteFaceSolver 白 OFF
		mf.move(outStr[0]);
		mf.showMF();
		return "test6";
	}
	
	public String test7() throws SQLException{
		String[] cols = { 
				"oggrwyygw",
				"rrwwyrybb",
				"wyybggboo",
				"gbgbbgwwb",
				"rygwroyoo", 
				"ryrrowoob",
				};
		mf.parseMFArray(cols);
		mf.showMF();
		
		//mFWriteFaceSolver On		
		mFWriteFaceSolver.setInvert(false);
		mFWriteFaceSolver.setDetail(true);
		mFWriteFaceSolver.setOriginalPosition(mf.extractMFSubStatus("00"),mf.extractMFSubStatus("01"));		
		//mFWriteFaceSolver.setOriginalPosition(cols,null);
		String[] outStr={""};
		if(mFWriteFaceSolver.solver(true)!=-1){
			mFWriteFaceSolver.arrangeStep(outStr);
//			System.out.println(outStr[0]);
		}
		//else continue;
		else return "test7";
		mFWriteFaceSolver.reset();
		//mFWriteFaceSolver OFF
		mf.move(outStr[0]);
		mf.showMF();
		
		String[] recoverOrder0={""};
		String[] recoverOrder1={""};
		if(!mFTwoFaceSolver2.solver(mf, recoverOrder0, recoverOrder1))
//			 continue;
			return "test7";
		//System.out.println(recoverOrder0[0]);
		//System.out.println(recoverOrder1[0]);
		mf.move(recoverOrder0[0]);
		mf.move(recoverOrder1[0]);
		mf.showMF();
		return "test7";
	}
	
	public String test8() throws SQLException{
		String[] cols = { 
				"oggrwyygw",
				"rrwwyrybb",
				"wyybggboo",
				"gbgbbgwwb",
				"rygwroyoo", 
				"ryrrowoob",
				};

		mf.parseMFArray(cols);
		mf.showMF();
		String[] u=mf.getMFDescription();
		List<String> orders=new ArrayList<String>();
		if(mFSolver.solver(cols, orders)){
			for(int i1=0;i1<orders.size();i1++){
				System.out.println(orders.get(i1));
				mf.move(orders.get(i1));
			}
		}
		mf.showMF();
		return "test8";		
	}
	
	public String getRecoverOrder() throws SQLException{
		String[] cols = colsForCube.split(";");
		try{
		mf.parseMFArray(cols);
		}catch(NullPointerException e){
			dataMap=new HashMap<String,String>();
			dataMap.put("order","404");
			return "getOrder";
		}
		String _historyOrder=convertOrder(historyOrder);
			mf.move(_historyOrder);

		
		List<String> orders=new ArrayList<String>();
		if(mFSolver.solver(cols, orders)){
			for(int i1=0;i1<orders.size();i1++){
				System.out.println(orders.get(i1));
				mf.move(orders.get(i1));
			}
		}
		StringBuffer resultStr=new StringBuffer();
		for(int i1=0;i1<orders.size();i1++){
			String[] orderArray=orders.get(i1).split(",");
			for(int i2=0;i2<orderArray.length;i2++){
				if(orderArray[i2].matches("([0-9])|(1[0-7])")){
					resultStr.append(orderArray[i2]);
					resultStr.append(",");
				}
			}
			
//			resultStr.append("\n");
		}
		resultStr.deleteCharAt(resultStr.length()-1);
		dataMap=new HashMap<String,String>();
		dataMap.put("order",convertOrder(resultStr.toString()));
		return "getOrder";
	}
	
	public String convertOrder(String order){
		String[] orders=order.split(",");
		Map<String,String> map=null;
		if(orders.length>0){
			if(orders[0].matches("[a-zA-Z]"))
			map=cubeOrderToMFOrder;
			else if("".equals(orders[0]))
				return "";
			else
			map=mFOrderToCubeOrder;
		}else return "";
			StringBuffer resultString=new StringBuffer();
		for(int i1=0;i1<orders.length;i1++){
			resultString.append(map.get(orders[i1]));
			if(i1!=orders.length-1){
				resultString.append(",");
			}
		}
		String str=resultString.toString();
			
		return str;
	}

}
