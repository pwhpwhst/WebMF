package Comparator;

import java.util.Comparator;

import pojo.MFStaticForScoreChangMain;
import pojo.MFSubStatus;
import pojo.MfStatisticForScoreChange;
import pojo.MfStatisticForTwoFace;

public class MfStatisticForScoreChangeMainComparator implements Comparator<MFStaticForScoreChangMain>{

	@Override
	public int compare(MFStaticForScoreChangMain arg0, MFStaticForScoreChangMain arg1) {	
		return arg0.getScore()-arg1.getScore();
	}

}
