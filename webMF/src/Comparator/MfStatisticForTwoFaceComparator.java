package Comparator;

import java.util.Comparator;

import pojo.MFSubStatus;
import pojo.MfStatisticForTwoFace;

public class MfStatisticForTwoFaceComparator implements Comparator<MfStatisticForTwoFace>{

	@Override
	public int compare(MfStatisticForTwoFace arg0, MfStatisticForTwoFace arg1) {	
		return arg0.getScore()-arg1.getScore();
	}

}
