package Comparator;

import java.util.Comparator;

import pojo.MFStaticForScoreChangMain;
import pojo.MfStatisticForScoreChange;

public class MfStatisticForScoreChangeComparator implements Comparator<MfStatisticForScoreChange>{

	@Override
	public int compare(MfStatisticForScoreChange o1,
			MfStatisticForScoreChange o2) {
		return 	o1.getScore()-o2.getScore();
	}

}
