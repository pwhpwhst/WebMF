package Comparator;

import java.util.Comparator;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import pojo.MFSubStatusParent;

public class MFSubStatusParentComparator implements Comparator<MFSubStatusParent> {

	@Override
	public int compare(MFSubStatusParent a, MFSubStatusParent b) {
		 if(a.getSubStatus()==b.getSubStatus())
			 return 0;
		 return a.getSubStatus()<b.getSubStatus()?-1:1;
	}

	public static void main(String[] arg)
	{
		Resource rs= new ClassPathResource("appcontext.xml");
		BeanFactory factory=new XmlBeanFactory(rs);
		
		MFSubStatusParentComparator mFSubStatusParentComparator = (MFSubStatusParentComparator) factory
				.getBean("MFSubStatusParentComparator");
		
	}
}
