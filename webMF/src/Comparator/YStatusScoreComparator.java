package Comparator;

import java.util.Comparator;

import pojo.YStatus;

public class YStatusScoreComparator implements Comparator<YStatus> {

	@Override
	public int compare(YStatus arg0, YStatus arg1) {
		// TODO Auto-generated method stub
		return arg0.getScore()-arg1.getScore();
	}

}
