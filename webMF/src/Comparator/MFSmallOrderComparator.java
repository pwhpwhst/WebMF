package Comparator;

import java.util.Comparator;
import java.util.Map;


public class MFSmallOrderComparator implements Comparator<Map<String,Object>> {

	@Override
	public int compare(Map<String, Object> arg0, Map<String, Object> arg1) {
		String s0=(String)arg0.get("SUBSTATUS");
		String s1=(String)arg1.get("SUBSTATUS");
		return s0.compareTo(s1);
	}

}
