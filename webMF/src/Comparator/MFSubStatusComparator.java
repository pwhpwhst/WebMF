package Comparator;
import java.util.Comparator;

import pojo.MFSubStatus;
import pojo.MFSubStatusParent;

public class MFSubStatusComparator implements Comparator<MFSubStatus> {

	@Override
	public int compare(MFSubStatus arg0, MFSubStatus arg1) {
		// TODO Auto-generated method stub
		if(arg0.getSubStatus()==arg1.getSubStatus())
			return 0;
		return arg0.getSubStatus()<arg1.getSubStatus()?-1:1;
	}
}