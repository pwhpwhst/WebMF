package Comparator;

import java.util.Comparator;
import java.util.Map;

import pojo.MFSubStatusParent;
import pojo.TwoSubStatus;

public class TwoSubStatusScoreComparator implements Comparator<TwoSubStatus>{

	@Override
	public int compare(TwoSubStatus arg0, TwoSubStatus arg1) {
//		return (twoSubStatusScore.get(arg0)%1000)-(twoSubStatusScore.get(arg1)%1000);
		return (twoSubStatusScore.get(arg0))-(twoSubStatusScore.get(arg1));		
	}
	

	private Map<TwoSubStatus,Integer> twoSubStatusScore=null;
	public void setTwoSubStatusScore(Map<TwoSubStatus, Integer> twoSubStatusScore) {
		this.twoSubStatusScore = twoSubStatusScore;
	}
	
}
