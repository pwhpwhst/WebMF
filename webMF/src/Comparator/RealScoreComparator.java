package Comparator;

import java.util.Comparator;

import pojo.RealScore;



public class RealScoreComparator implements Comparator<RealScore> {
	public int compare(RealScore arg0,RealScore arg1){
			return arg0.getScore()-arg1.getScore();
	}
}
