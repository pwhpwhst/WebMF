package Comparator;

import java.util.Comparator;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import pojo.MFSubStatusParent;

public class MFSubStatusScoreComparator implements Comparator<MFSubStatusParent> {

	@Override
	public int compare(MFSubStatusParent a, MFSubStatusParent b) {
		 if(a.getScore()==b.getScore())
			 return 0;
		 return a.getScore()<b.getScore()?-1:1;
	}

}
