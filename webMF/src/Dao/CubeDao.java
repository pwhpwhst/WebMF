package Dao;

import java.util.ArrayList;
import java.util.List;

import org.springframework.orm.ibatis.support.SqlMapClientDaoSupport;

import com.ibatis.sqlmap.client.SqlMapClient;

public class CubeDao extends SqlMapClientDaoSupport {
	
	public List queryForList(String sqlID){
		List list=null;
		list=getSqlMapClientTemplate().queryForList(sqlID);
		if(list==null){
			list=new ArrayList();
		}
		return list;
	}
	
	public Object queryForObject(String sqlID,Object object){
		return getSqlMapClientTemplate().queryForObject(sqlID, object);
	}
}
