package Solver;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import pojo.YStatus;

import com.ibatis.sqlmap.client.SqlMapClient;

import Comparator.YStatusComparator;
import Dao.CubeDao;
import MFCode.MFCode;

public class MFTwoFaceSolver2 {
//public SqlMapClient sqlMap;
public CubeDao cubeDao;
public Comparator<YStatus> yStatusComparator;
private List<YStatus> recoverOrderList=null;

public List<YStatus> queryRecoverOrderList() throws SQLException{
	if(recoverOrderList==null){
		recoverOrderList=cubeDao.queryForList("MF_RECOVER_YF_ORDER_SMALL_SELECT3");
	Collections.sort(recoverOrderList, yStatusComparator);}
	return recoverOrderList;
}

public List<YStatus> createNextStatusList(List<YStatus> recoverList,MFCode mf,String beforeOrder,String afterOrder,boolean isOrderInvert){
	List<YStatus> nextSubstatusList=new ArrayList<YStatus>();
	YStatus _yStatus=null;
	for(int i1=0;i1<recoverList.size();i1++){
		if(recoverList.get(i1).getScore()<=1)
			continue;
		String order=recoverList.get(i1).getNextRecoverOrder();
		String _a=("".equals(beforeOrder))?"":",";
		String _b=("".equals(afterOrder))?"":",";
		String _c=(" ".equals(order))?"":",";
		order=beforeOrder+_a+order+_c+afterOrder+_b;
		if(isOrderInvert)
			order=invertOrder(order);
		int length=recoverList.get(i1).getScore();
		int _add=0;
		if(!"".equals(beforeOrder)) _add++;
		if(!"".equals(afterOrder)) _add++;
		length+=_add;
		mf.move(order);
		int subStatus03=mf.extractMFSubStatus("03");
		int subStatus04=mf.extractMFSubStatus("04");
		String subStatus=subStatus03+","+subStatus04;
		_yStatus=new YStatus();
		_yStatus.setSubstatus(subStatus);
		_yStatus.setScore(length);
		_yStatus.setNextRecoverOrder(order);
		mf.remove(order);
		nextSubstatusList.add(_yStatus);
	}
	Collections.sort(nextSubstatusList, yStatusComparator);
	return nextSubstatusList;
}

public static String invertOrder(String order){
	String[] s=order.split(",");
	int[] u={1,0,2,4,3,5,7,6,8,10,9,11,13,12,14,16,15,17};
	String _s="";
	for(int i1=s.length-1;i1>=0;i1--){
		if(s[i1].matches("\\s*")) continue;
		_s+=u[Integer.valueOf(s[i1])]+",";
	}
	return _s;
}


public boolean solver(MFCode mf,String[] recoverOrder0,String[] recoverOrder1) throws SQLException{
	List<YStatus> recoverList=queryRecoverOrderList();
	List<YStatus> nextStatusList=new ArrayList<YStatus>();
	String[] predealOrder={"","3","4","5"};
	boolean[] isInvert={false,true};
	for(int i1=0;i1<2;i1++){
		for(int i2=0;i2<4;i2++){
			for(int i3=0;i3<4;i3++){
				List<YStatus>_nextStatusList=createNextStatusList(recoverList, mf,predealOrder[i2],predealOrder[i3],isInvert[i1]);
				nextStatusList.addAll(_nextStatusList);
			}
		}
	}
	
	nextStatusList.retainAll(recoverList);
	if(nextStatusList.size()==0){
		return false;
	}
	int minstep=999;
	int minid=-1;
	for(int i1=0;i1<nextStatusList.size();i1++){
		int a1=nextStatusList.get(i1).getScore();
		int id=Collections.binarySearch(recoverList, nextStatusList.get(0), yStatusComparator);
		int a2=recoverList.get(id).getScore();
		if((a1+a2)<minstep){
			minstep=a1+a2;
			minid=i1;
		}
	}
	int id=Collections.binarySearch(recoverList, nextStatusList.get(minid), yStatusComparator);
	recoverOrder0[0]=nextStatusList.get(minid).getNextRecoverOrder();
	recoverOrder1[0]=recoverList.get(id).getNextRecoverOrder();
	return true;
}
//	public static void main(String[] arg) throws SQLException{
//		Resource rs= new ClassPathResource("appcontext.xml");
//		BeanFactory factory=new XmlBeanFactory(rs); 
//		Comparator<YStatus> yStatusComparator=(Comparator<YStatus>)factory.getBean("YStatusComparator");
//		MFWriteFaceSolver mFWriteFaceSolver=(MFWriteFaceSolver)factory.getBean("MFWriteFaceSolver");
//		MFTwoFaceSolver2 mFTwoFaceSolver2=(MFTwoFaceSolver2)factory.getBean("MFTwoFaceSolver2");
//		String[] cols = { 
//		"oggrwyygw",
//		"rrwwyrybb",
//		"wyybggboo",
//		"gbgbbgwwb",
//		"rygwroyoo", 
//		"ryrrowoob",
//};	 
//		
//		MFCode mf=(MFCode)factory.getBean("MFCodeImpl");
//		mf.parseMFArray(cols);
//		
////mFWriteFaceSolver On		
//		mFWriteFaceSolver.setInvert(false);
//		mFWriteFaceSolver.setDetail(true);
//		mFWriteFaceSolver.setOriginalPosition(mf.extractMFSubStatus("00"),mf.extractMFSubStatus("01"));		
////		mFWriteFaceSolver.setOriginalPosition(cols,null);
//		String[] outStr={""};
//		if(mFWriteFaceSolver.solver(true)!=-1){
//			mFWriteFaceSolver.arrangeStep(outStr);
//			System.out.println(outStr[0]);
//		}
////mFWriteFaceSolver OFF
//		mf.move(outStr[0]);
//		
//		String[] recoverOrder0={""};
//		String[] recoverOrder1={""};
//		mFTwoFaceSolver2.solver(mf, recoverOrder0, recoverOrder1);
//		System.out.println(recoverOrder0[0]);
//		System.out.println(recoverOrder1[0]);
//		mf.move(recoverOrder0[0]);
//		mf.move(recoverOrder1[0]);
//		
//		mf.showMF();
//		System.out.println(mf.extractMFSubStatus("03"));
//		System.out.println(mf.extractMFSubStatus("04"));
//		//826028852,1027553077
//}


public static void main(String[] arg) throws SQLException{
Resource rs= new ClassPathResource("appcontext.xml");
BeanFactory factory=new XmlBeanFactory(rs); 
Comparator<YStatus> yStatusComparator=(Comparator<YStatus>)factory.getBean("YStatusComparator");
MFWriteFaceSolver mFWriteFaceSolver=(MFWriteFaceSolver)factory.getBean("MFWriteFaceSolver");
MFTwoFaceSolver2 mFTwoFaceSolver2=(MFTwoFaceSolver2)factory.getBean("MFTwoFaceSolver2");
String[] cols = { 
"oggrwyygw",
"rrwwyrybb",
"wyybggboo",
"gbgbbgwwb",
"rygwroyoo", 
"ryrrowoob",
};	 

MFCode mf=(MFCode)factory.getBean("MFCodeImpl");
mf.parseMFArray(cols);
Random ram=new Random();
Set<String> set=new HashSet<String>();
//for(int i2=0;i2<2000;i2++){
//	System.out.println("第"+i2+"次");
for(int i1=0;i1<50;i1++){
	int a=ram.nextInt();
	a=(a>0)?a:-a;
	mf.move(a%18);
}
//mFWriteFaceSolver On		
mFWriteFaceSolver.setInvert(false);
mFWriteFaceSolver.setDetail(true);
mFWriteFaceSolver.setOriginalPosition(mf.extractMFSubStatus("00"),mf.extractMFSubStatus("01"));		
//mFWriteFaceSolver.setOriginalPosition(cols,null);
String[] outStr={""};
if(mFWriteFaceSolver.solver(true)!=-1){
	mFWriteFaceSolver.arrangeStep(outStr);
//	System.out.println(outStr[0]);
}
//else continue;
else return;
mFWriteFaceSolver.reset();
//mFWriteFaceSolver OFF
mf.move(outStr[0]);
//mf.showMF();
String[] recoverOrder0={""};
String[] recoverOrder1={""};
if(!mFTwoFaceSolver2.solver(mf, recoverOrder0, recoverOrder1))
//	 continue;
	return;
//System.out.println(recoverOrder0[0]);
//System.out.println(recoverOrder1[0]);
mf.move(recoverOrder0[0]);
mf.move(recoverOrder1[0]);

//mf.showMF();
//System.out.println(mf.extractMFSubStatus("03"));
//System.out.println(mf.extractMFSubStatus("04"));
set.add(String.valueOf(mf.extractMFSubStatus("02")));
//}

Iterator<String> it=set.iterator();
System.out.println("共"+set.size()+"情况");
while(it.hasNext()){
	System.out.println(it.next());
}
//826028852,1027553077
}

	public CubeDao getCubeDao() {
	return cubeDao;
}

public void setCubeDao(CubeDao cubeDao) {
	this.cubeDao = cubeDao;
}

	public Comparator<YStatus> getyStatusComparator() {
		return yStatusComparator;
	}

	public void setyStatusComparator(Comparator<YStatus> yStatusComparator) {
		this.yStatusComparator = yStatusComparator;
	}
}

//1330447623
