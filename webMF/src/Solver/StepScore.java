package Solver;

public class StepScore {
	private int step;
	public int getStep() {
		return step;
	}
	public void setStep(int step) {
		this.step = step;
	}
	
	private int score;
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	
	StepScore() {
		this.step = 18;
		this.score = 0;
	}		
}
