package Solver;

import java.sql.SQLException;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import Solver.Common;
import com.ibatis.sqlmap.client.SqlMapClient;

import pojo.TwoSubStatus;
import pojo.TwoSubStatusScore;

import Dao.CubeDao;
import MFCode.MFCode;

public class MFTwoSubStatusSolver {
	
	private MFCode mf00;
	public MFCode getMf00() {
		return mf00;
	}

	public void setMf00(MFCode mf00) {
		this.mf00 = mf00;
	}

	public MFCode getMf01() {
		return mf01;
	}

	public void setMf01(MFCode mf01) {
		this.mf01 = mf01;
	}

	public MFSubStatusBuilder getBuilder00() {
		return builder00;
	}

	public void setBuilder00(MFSubStatusBuilder builder00) {
		this.builder00 = builder00;
	}

	public MFSubStatusBuilder getBuilder01() {
		return builder01;
	}

	public void setTwoSubStatusMap(TwoSubStatusMap twoSubStatusMap) {
		this.twoSubStatusMap = twoSubStatusMap;
	}

	private MFCode mf01;
	private MFSubStatusBuilder builder00;
	private MFSubStatusBuilder builder01;
//	private SqlMapClient sqlMap;
//	private Map<TwoSubStatus,Integer> twoSubStatusMap;
	private TwoSubStatusMap twoSubStatusMap;
	private boolean isInvert;
	
	public void setInvert(boolean isInvert) {
		this.isInvert = isInvert;
	}
	


//	public MFTwoSubStatusSolver(MFCode mf00, MFCode mf01,MFSubStatusBuilder builder00,
//			MFSubStatusBuilder builder01)
//	{
//		this.mf00=mf00;
//		this.mf01=mf01;
//		this.builder00=builder00;
//		this.builder01=builder01;	
//	}
	
	public void initTwoMFs(int subStatus00,int subStatus01)
	{ 	
    	builder00.builder(subStatus00, mf00,isInvert);
    	builder01.builder(subStatus01, mf01,isInvert);
	}
	
	public String findNextStep(int depth,boolean isFirst,int vscore)
	{
		int[] stepConvert={3,4,5,0,1,2,9,10,11,6,7,8,15,16,17,12,13,14};
		int subStatus00=mf00.extractMFSubStatus("00",isInvert);
		if(isInvert)
		subStatus00=Common.invertSubstatus(subStatus00);
		int subStatus01=mf01.extractMFSubStatus("01",isInvert);
		if(isInvert)
		subStatus01=Common.invertSubstatus(subStatus01);
		TwoSubStatus a=new TwoSubStatus(subStatus00,subStatus01);
		
		Integer score=null;
		if(isFirst==true)
		  score = twoSubStatusMap.getTwoSubStatusMap().get(a);
		else
			score=vscore;
		
		
		if(score==0)
			return "";
		
		String _str=null;
		for(int i1=0;i1<18;i1++)
		{
			mf00.move(i1);mf01.move(i1);
			 subStatus00=mf00.extractMFSubStatus("00",isInvert);
				if(isInvert)
					subStatus00=Common.invertSubstatus(subStatus00);
			 subStatus01=mf01.extractMFSubStatus("01",isInvert);
				if(isInvert)
					subStatus01=Common.invertSubstatus(subStatus01);			 
			
			 TwoSubStatus b=new TwoSubStatus(subStatus00,subStatus01);
			 Integer childScore=twoSubStatusMap.getTwoSubStatusMap().get(b);
			 
			 if(childScore!=null)
			 {
				 if(childScore<=(score-1))
				 {
					 _str=Integer.valueOf(i1).toString();
					 mf00.remove(i1);mf01.remove(i1);
					 return _str;
				 } 
			 }

				mf00.remove(i1);mf01.remove(i1);
		}
		
		for(int i1=0;i1<18;i1++)
		{
			mf00.move(i1);mf01.move(i1);
			 subStatus00=mf00.extractMFSubStatus("00",isInvert);
				if(isInvert)
					subStatus00=Common.invertSubstatus(subStatus00);
			 subStatus01=mf01.extractMFSubStatus("01",isInvert);
				if(isInvert)
					subStatus01=Common.invertSubstatus(subStatus01);
			 TwoSubStatus b=new TwoSubStatus(subStatus00,subStatus01);
			 Integer childScore=twoSubStatusMap.getTwoSubStatusMap().get(b);
			 
			 if(childScore==null)
			 {
				 if(depth!=0)
				 {
					 _str=findNextStep(depth-1,false,score-1);
					if (_str != null) {
						if(!"".equals(_str))
						{
							_str = Integer.valueOf(i1).toString()+","+ _str;							
						}

						mf00.remove(i1);mf01.remove(i1);
						break;
					}
				 }
			 }
				mf00.remove(i1);mf01.remove(i1);
		}
		return _str;
	}
	
	public String solve()
	{
		int  subStatus00=mf00.extractMFSubStatus("00",isInvert);
		if(isInvert)
			subStatus00=Common.invertSubstatus(subStatus00);
		int  subStatus01=mf01.extractMFSubStatus("01",isInvert);
		if(isInvert)
			subStatus01=Common.invertSubstatus(subStatus01);
		String str="";
		String _str="";
		String[] _strs=null;
		while(!((subStatus00==286528284)&&(subStatus01==353836829)))
		{
			TwoSubStatus a=new TwoSubStatus(subStatus00,subStatus01);
			int score = twoSubStatusMap.getTwoSubStatusMap().get(a);
//			System.out.println("目前得分为："+score);
			
			_str=findNextStep(3,true,0);
			_strs=_str.split(",");
			for(int i1=0;i1<_strs.length;i1++)
			{
				mf00.move(Integer.parseInt(_strs[i1]));
				mf01.move(Integer.parseInt(_strs[i1]));
			}
			  subStatus00=mf00.extractMFSubStatus("00",isInvert);
			  if(isInvert)
			  subStatus00=Common.invertSubstatus(subStatus00);
			  subStatus01=mf01.extractMFSubStatus("01",isInvert);
			  if(isInvert)
			  subStatus01=Common.invertSubstatus(subStatus01);
			  if("".equals(str))
				  str=_str;
			  else
			str=str+","+_str;
		}
		return str;
	}
	
	public int getScore(){
		int  subStatus00=mf00.extractMFSubStatus("00",isInvert);
		if(isInvert)
			subStatus00=Common.invertSubstatus(subStatus00);
		int  subStatus01=mf01.extractMFSubStatus("01",isInvert);
		if(isInvert)
			subStatus01=Common.invertSubstatus(subStatus01);
		
		TwoSubStatus a=new TwoSubStatus(subStatus00,subStatus01);
		int score = twoSubStatusMap.getTwoSubStatusMap().get(a);
		return score;
	}
	
	
//	public static void main(String[] args)
//	{
//		Resource rs= new ClassPathResource("appcontext.xml");
//		BeanFactory factory=new XmlBeanFactory(rs);
//		
//		MFTwoSubStatusSolver mFTwoSubStatusSolver = (MFTwoSubStatusSolver) factory
//				.getBean("MFTwoSubStatusSolver");
//		
//		mFTwoSubStatusSolver.initTwoMFs(1292175187,353836829);
//		String str=mFTwoSubStatusSolver.solve();
////		9,12,6,0,12,4,12,15,4,12,17,4,15,11,8,13,8,11
//		System.out.println("s");
//	}
	
	public void setBuilder01(MFSubStatusBuilder builder01) {
		this.builder01 = builder01;
	}

	public static void main(String[] args) throws SQLException{
		Resource rs= new ClassPathResource("appcontext.xml");
		BeanFactory factory=new XmlBeanFactory(rs);
		
		
		MFTwoSubStatusSolver mFTwoSubStatusSolver = (MFTwoSubStatusSolver) factory
		.getBean("MFTwoSubStatusSolver");
		mFTwoSubStatusSolver.setInvert(false);
			
		SqlMapClient sqlMap=(SqlMapClient)factory.getBean("SqlMap");
		List<TwoSubStatusScore> list=null;
		try {
			list=sqlMap.queryForList("MF_TWOSUBSTATUS_SCORE_SELECT");
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		for(int i1=0;i1<list.size();i1++){
			int subStatus00=list.get(i1).getSubStatus00();
			int subStatus01=list.get(i1).getSubStatus01();
			mFTwoSubStatusSolver.initTwoMFs(subStatus00,subStatus01);
			String str=mFTwoSubStatusSolver.findNextStep(2, true, 0);
			list.get(i1).setNextStep(str);
		}
		
		sqlMap.startBatch();
		for(int i1=0;i1<list.size();i1++){
			try {
				sqlMap.update("MF_TWOSUBSTATUS_SCORE_UPDATE_NEXT_STEP", list.get(i1));
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		sqlMap.executeBatch();
			
	}
	
}
