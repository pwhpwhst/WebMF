package Solver;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import Dao.CubeDao;

import pojo.TwoSubStatus;
import pojo.TwoSubStatusScore;

public class TwoSubStatusMap {
	private Map<TwoSubStatus,Integer> twoSubStatusMap;
	
	public TwoSubStatusMap(CubeDao cubeDao){
		System.out.println("开始创建twoSubStatusMap...");
		long startTime=System.currentTimeMillis();
		List<TwoSubStatusScore> list=null;
			list=cubeDao.queryForList("MF_TWOSUBSTATUS_SCORE_SELECT");

		
		twoSubStatusMap=new TreeMap<TwoSubStatus,Integer>();
		for(int i1=0;i1<list.size();i1++)
		{
			TwoSubStatusScore a=list.get(i1);
			TwoSubStatus _b=new TwoSubStatus(a.getSubStatus00(),a.getSubStatus01());
			twoSubStatusMap.put(_b, Integer.valueOf(a.getScore()));
		}
		long endTime=System.currentTimeMillis();
		System.out.println("创建结束，耗时"+(endTime-startTime)/1000+"秒");
	}

	public Map<TwoSubStatus, Integer> getTwoSubStatusMap() {
		return twoSubStatusMap;
	}
	
	 
	
}
