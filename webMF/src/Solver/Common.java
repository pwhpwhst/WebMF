package Solver;

public class Common {
	public static int invertSubstatus(int subStatus){
		int[] _convert={0,3,2,1};
		int[] _convert2={255,65280,16711680,2130706432};
		int[] _convert3={3,12,48,192};
		int[] _order={0,1,2,3};
		boolean[] _forword={true,false,true,false};
//		int subStatus=-2147483648;
//		int u1=(subStatus>0)?subStatus:-(subStatus+1);
		int u1=(subStatus>0)?subStatus:subStatus+1073741824+1073741824;
		int newU1=0;
		int u2=0;
		int newU2=0;
		for(int i1=0;i1<=3;i1++){
			 u2=(u1&_convert2[i1])>>(i1*8);
				newU2=0;
				boolean isZAxisEqualsZero=false;
				boolean isTwoGridType=false;
				int _x=0;
			for(int i2=0;i2<=2;i2++){
				if(i2!=0){
					_x=_convert[(u2&_convert3[i2])>>(i2*2)]<<(i2*2);
					isTwoGridType=(_x==0)?true:isTwoGridType;
					newU2|=_x;

				}else{
					 _x=u2&_convert3[i2];
					 isTwoGridType=(_x==0)?true:isTwoGridType;
					 isZAxisEqualsZero=(_x==0)?true:false;
					newU2|=_x;//Z轴的数据不需要转化
				}
			}
			if(isTwoGridType)
			newU2|=(isZAxisEqualsZero^_forword[i1])?(u2&(192))^64:u2&(192);
			else
				newU2|=u2&(192);
			newU1|=newU2<<(_order[i1]*8);
		}
		return (subStatus>0)?newU1:newU1-1073741824-1073741824;
		}
}
