package Solver;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Stack;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import pojo.MFSubStatusList;
import pojo.MFSubStatusParent;


import MFCode.MFCode;
import MFCode.MFCodeImpl;
/**
 * 根据给定的SubStatus构建任意的魔方*/
public class MFSubStatusBuilder {

	private Stack<BigDecimal> steps;
	private MFSubStatusList  mFSubStatusList;
	private Comparator<MFSubStatusParent> comparator=null;
	
	public MFSubStatusBuilder() {
		steps = new Stack<BigDecimal>();
	}
	
	public void setMFSubStatusList(MFSubStatusList  mFSubStatusList) {
		this.mFSubStatusList = mFSubStatusList;
	}

	public void setComparator(Comparator<MFSubStatusParent> comparator) {
		this.comparator = comparator;
	}

	public MFSubStatusList getmFSubStatusList() {
		return mFSubStatusList;
	}

	public void builder(int subStatus,MFCode mf,boolean isInvert)
	{
		solver(mf,false,isInvert);
		getStepsFromSubStatus(subStatus,isInvert);	
		while(!steps.isEmpty())
			mf.remove(steps.pop().intValue());
	}
	
	private void solver (MFCode mf,boolean isInner,boolean isInvert)
	{
		if(isInner==false)
		{
			getStepsFromSubStatus(mf.extractMFSubStatus(mFSubStatusList.getType(),isInvert),isInvert);
			solver(mf,true,isInvert);		
		}
		else
		{
			int nextStep;
			if(this.steps.size()==0)
				return;
			else {
				nextStep = steps.pop().intValue();
				solver(mf, true,isInvert);
				mf.move(nextStep);
			}
		}
	}
	
	
	private void getStepsFromSubStatus(int subStatus,boolean isInvert)
	{ 
		MFSubStatusParent mFSubStatusParent=new MFSubStatusParent();
		List<MFSubStatusParent> list=mFSubStatusList.getList();
		int endSubStatus=0;
		if("00".equals(mFSubStatusList.getType()))
			endSubStatus=286528284;
		else if("01".equals(mFSubStatusList.getType()))
			endSubStatus=353836829;
		
		if(isInvert)
			subStatus=mFSubStatusList.invertSubstatus(subStatus);
		
		while(subStatus!=endSubStatus)
		{
			mFSubStatusParent.setSubStatus(subStatus);
			int index=Collections.binarySearch(list, mFSubStatusParent,comparator);
			int parentMove=list.get(index).getParentMove();
			if(isInvert)
				parentMove=invertMove(parentMove);
			steps.push(BigDecimal.valueOf(parentMove));
			subStatus=list.get(index).getParentSubStatus();			
		}
	}
	
	private int invertMove(int move){
		int[] a={3,4,5,0,1,2,9,10,11,6,7,8,12,13,14,15,16,17};
		return a[move];
	}
	
//	public void getAllSolveOrder(String type,MFCode mf,List<String> outputOrders){
//		int presentScore=mFSubStatusList.getScore(mf.extractMFSubStatus(type, false), false);
//		if(presentScore==0){
//			outputOrders.add("");
//			return;
//		}
//		for(int i1=0;i1<18;i1++){
//			mf.move(i1);
//			int nextSubStatus=mf.extractMFSubStatus(type, false);
//			int nextScore=mFSubStatusList.getScore(nextSubStatus, false);
//			if(nextScore<presentScore){
//				List<String> _outputOrders=new ArrayList<String>();
//				getAllSolveOrder(type,mf,_outputOrders);
//				for(int i2=0;i2<_outputOrders.size();i2++){
//					_outputOrders.set(i2, i1+","+_outputOrders.get(i2));
//				}
//				outputOrders.addAll(_outputOrders);
//			}
//			mf.remove(i1);
//		}
//	}
	
	public boolean getAllSolveOrder(String type,MFCode mf,List<String> outputOrders,int limitScore){
		int presentScore=mFSubStatusList.getScore(mf.extractMFSubStatus(type, false), false);
		if(presentScore>limitScore){
			return false;
		}
		if(presentScore==0){
			outputOrders.add("");
			return true;
		}
		for(int i1=0;i1<18;i1++){
			mf.move(i1);
			List<String> _outputOrders=new ArrayList<String>();
			if(getAllSolveOrder(type,mf,_outputOrders,limitScore-1)){
				for(int i2=0;i2<_outputOrders.size();i2++){
				_outputOrders.set(i2, i1+","+_outputOrders.get(i2));
			}
			outputOrders.addAll(_outputOrders);
			}
			mf.remove(i1);
		}
		return true;
	}
	
	public void getAllSolveOrder(String type,int subStatus,List<String> outputOrders,BeanFactory factory,int limitScore){
		MFCode mf=(MFCode) factory.getBean("MFCodeImpl");
		builder(subStatus, mf, false);
		
		getAllSolveOrder(type,mf,outputOrders,limitScore);
	}
	
	public static void main(String[] args)
	{
		Resource rs= new ClassPathResource("appcontext.xml");
		BeanFactory factory=new XmlBeanFactory(rs);
		
		MFSubStatusBuilder mFSubStatusBuilder=(MFSubStatusBuilder)factory.getBean("MFSubStatusBuilder00");
		MFCode mf=(MFCode) factory.getBean("MFCodeImpl");
		int subStatus=84349724;
		
		mFSubStatusBuilder.builder(subStatus, mf, false);
		mf.move(3);
		int nextSubStatus=mf.extractMFSubStatus("00", false);
		mf.remove(3);
		
		List<String> outputOrders=new ArrayList<String>();
		if(nextSubStatus==subStatus){
		mFSubStatusBuilder.getAllSolveOrder("00", subStatus, outputOrders, factory,3);
		}
		System.out.println("解法有"+outputOrders.size()+"种");
		for(int i1=0;i1<outputOrders.size();i1++){
			System.out.println(outputOrders.get(i1));
		}
//		String[] cols = { 
//				"rbobwwrgo",
//				"wwywyyryw",
//				"wygrggggy",
//				"obyrbrbbw",
//				"booorggyb", 
//				"grywoorob",
//		};
//		
//		MFCode mf=(MFCode)factory.getBean("MFCodeImpl");
//		mf.parseMFArray(cols);
//		
//		mFSubStatusBuilder.builder(286528284, mf);
//		mf.showMF();
		

	}
	
	
}
