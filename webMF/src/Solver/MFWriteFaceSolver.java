package Solver;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;
import java.util.Stack;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import pojo.MFSubStatusList;
import pojo.TwoSubStatus;

import MFCode.MFCode;
import MFCode.MFCodeStatic;



/**
 * */
public class MFWriteFaceSolver {
	//注入bean	
//	private CreatMFScoreTable creatMFScoreTable;	
//	public void setCreatMFScoreTable(CreatMFScoreTable creatMFScoreTable) {
//		this.creatMFScoreTable = creatMFScoreTable;
//	}
	public static final String TYPE00="00";
	public static final String TYPE01="01";
	public static final String TYPE02="02";
	public static final String TYPE03="03";
	public static final String TYPE04="04";
	
	private MFSubStatusList mFSubStatusList00;
	private MFSubStatusList mFSubStatusList01;
	private MFTwoSubStatusSolver mFTwoSubStatusSolver;
	private String twoSubStatusResult=null;
	private boolean isInvert;
	private boolean isDetail;
	private int twoSubStatusScore;
	private String type00;
	private String type01;
//	public MFCode mf;
	public MFCode mf00;
	public MFCode mf01;
	private Stack<Step> steps=null;
	private Set<TwoSubStatus> trace=null; 
	private MFSubStatusBuilder builder00;
	private MFSubStatusBuilder builder01;
//	private Map<Integer,Float> selectBadProbability;
//	public static int[]={
//		
//	}
	
	public MFSubStatusBuilder getBuilder00() {
		return builder00;
	}

	public void setBuilder00(MFSubStatusBuilder builder00) {
		this.builder00 = builder00;
	}

	public MFSubStatusBuilder getBuilder01() {
		return builder01;
	}

	public void setBuilder01(MFSubStatusBuilder builder01) {
		this.builder01 = builder01;
	}

	public MFSubStatusList getmFSubStatusList00() {
		return mFSubStatusList00;
	}

	public void setmFSubStatusList00(MFSubStatusList mFSubStatusList00) {
		this.mFSubStatusList00 = mFSubStatusList00;
	}
		
	public MFSubStatusList getmFSubStatusList01() {
		return mFSubStatusList01;
	}

	public void setmFSubStatusList01(MFSubStatusList mFSubStatusList01) {
		this.mFSubStatusList01 = mFSubStatusList01;
	}

	
	public void setmFTwoSubStatusSolver(MFTwoSubStatusSolver mFTwoSubStatusSolver) {
		this.mFTwoSubStatusSolver = mFTwoSubStatusSolver;
	}



//实体成员

//	public void setMf(MFCode mf) {
//		this.mf = mf;
//	}
	
	public void setMf00(MFCode mf) {
		this.mf00 = mf;
	}
	
	public void setMf01(MFCode mf) {
		this.mf01 = mf;
	}
	
	public void setInvert(boolean isInvert) {
		this.isInvert = isInvert;
		if(isInvert){
			type00=TYPE03;
			type01=TYPE04;
		}else{
			type00=TYPE00;
			type01=TYPE01;			
		}		
	}
	
	public void setDetail(boolean isDetail){
			this.isDetail=isDetail;
	}

	public void setOriginalPosition(String[] cols,List<Integer> moveOrder)
	{
		 mf00.parseMFArray(cols); 
		 mf01.parseMFArray(cols); 
		if(moveOrder!=null){
			for(int i1=0;i1<moveOrder.size();i1++){
				mf00.move(moveOrder.get(i1));
				mf01.move(moveOrder.get(i1));
			}
		}
		int subStatus00=mf00.extractMFSubStatus(type00);
		int subStatus01=mf01.extractMFSubStatus(type01);
		trace.add(new TwoSubStatus(subStatus00,subStatus01));
	}
	
	public void setOriginalPosition(int subStatus00,int subStatus01){
		builder00.builder(subStatus00, mf00, isInvert);
		builder01.builder(subStatus01, mf01, isInvert);
		trace.add(new TwoSubStatus(subStatus00,subStatus01));
	}
	
	public void setOriginalPosition(int subStatus00,int subStatus01,String order){
		builder00.builder(subStatus00, mf00, isInvert);
		mf00.move(order);
		builder01.builder(subStatus01, mf01, isInvert);
		mf01.move(order);
		if(!isInvert){
			trace.add(new TwoSubStatus(mf00.extractMFSubStatus("00"),mf00.extractMFSubStatus("01")));			
		}else{
			trace.add(new TwoSubStatus(mf00.extractMFSubStatus("03"),mf00.extractMFSubStatus("04")));			
		}
	}
	
	
	public void setOriginalPosition(String[] cols){
		setOriginalPosition(cols,null);
	}
	

//操作方法
	public int findNextStep(boolean otherMoth)
	{
//		 boolean isEndurate=false; 
		 int[] score00=new int[18];
		 int[] score01=new int[18];
		 
		 List<StepScore> stepScores=new ArrayList<StepScore>(18);
		 initStepScores(stepScores);
		 
		if(steps.size()>=21)
			return 18;
		int presentScore00=mFSubStatusList00.getScore(mf00.extractMFSubStatus(type00),isInvert);
		int presentScore01=mFSubStatusList01.getScore(mf01.extractMFSubStatus(type01),isInvert);
		int presentTotalScore=presentScore00+presentScore01;

		for(int i1=0;i1<=17;i1++)
		{
//			mf.move(i1);
			mf00.move(i1);
			mf01.move(i1);
			score00[i1]=mFSubStatusList00.getScore(mf00.extractMFSubStatus(type00),isInvert);
			score01[i1]=mFSubStatusList01.getScore(mf01.extractMFSubStatus(type01),isInvert);
			stepScores.get(i1).setStep(i1);
			stepScores.get(i1).setScore(score00[i1]+score01[i1]);
//			System.out.println(i1+":"+score00[i1]+":"+score01[i1]+":"+stepScores.get(i1).getScore());
			mf00.remove(i1);
			mf01.remove(i1);

		}
		Collections.sort(stepScores, stepScoreComparator);	
		boolean hasScoreDown=false;
		
		int minDepth=100; 
		int[] _betterGrandStep=new int[1];
		int betterStep=18;
		int difference=100;
		boolean hasBetterStep=false;
		for(int i1=0;i1<=17;i1++)
		{
			int step=stepScores.get(i1).getStep();
			int score=stepScores.get(i1).getScore();
			
			mf00.move(step);
			mf01.move(step);
			if (hasTwoSubStatus(mf00,mf01)) {
				mf00.remove(step);
				mf01.remove(step);
				continue;
			} else {
				mf00.remove(step);
				mf01.remove(step);
			}
			
			if(!steps.peek().isStopStep(step))
			{
				if(score>(40-2*(steps.size()+1)))
				{
					steps.peek().setStopBit(step);
					continue;
				}

				if (score < presentTotalScore) {
					hasScoreDown=true;
//						return step;
					if(betterStep==18)
					{
						betterStep=step;
						difference=score00[i1]-score01[i1];
						difference=(difference>=0)?difference:-1*difference;
					}
					else
					{
						int presentDifference=score00[i1]-score01[i1];
						presentDifference=(presentDifference>=0)?presentDifference:-1*presentDifference;
						if(presentDifference<difference)
							{difference=presentDifference;
						betterStep=step;}
					}
				}
				else if((hasScoreDown==false)&&(score == presentTotalScore))
				{
					if(hasBetterStep==false){
						hasBetterStep=true;
						if(otherMoth)
						betterStep=step;
					}
						
					mf00.move(step);
					mf01.move(step);
					int depth=findScoreDepth(2, _betterGrandStep);
					mf00.remove(step);
					mf01.remove(step);
					minDepth=minDepth<depth?minDepth:depth;
					betterStep=minDepth<depth?betterStep:step;
				}
				else {
					steps.peek().setStopBit(step);
				}

			}
		}
		return betterStep;
	}
	



	//正常情况下设置为false
	public int solver(boolean otherMode)
	{
//		long startTimeA=System.currentTimeMillis();
		int prsentScore00=mFSubStatusList00.getScore(mf00.extractMFSubStatus(type00),isInvert);
		int prsentScore01=mFSubStatusList01.getScore(mf01.extractMFSubStatus(type01),isInvert);
		int prsentScore=prsentScore00+prsentScore01;
		while(prsentScore>4)
		{

//			System.out.println("目前的分数："+prsentScore+","+prsentScore00+","+prsentScore01);
			int nextStep=findNextStep(otherMode);

			if(nextStep!=18)
			{
				go(nextStep);
//				System.out.println("尝试"+nextStep+",已走步数："+steps.size());


				 prsentScore00=mFSubStatusList00.getScore(mf00.extractMFSubStatus(type00),isInvert);
				 prsentScore01=mFSubStatusList01.getScore(mf01.extractMFSubStatus(type01),isInvert);			
				 prsentScore=prsentScore00+prsentScore01;	
			}

			else
				{	if(-1==back()) {
					if(otherMode==false){
						boolean _isDetail=isDetail;
						reset();
						isDetail=_isDetail;
						return solver(true);
					}else return -1;					
					}else{
					 prsentScore00=mFSubStatusList00.getScore(mf00.extractMFSubStatus(type00),isInvert);
					 prsentScore01=mFSubStatusList01.getScore(mf01.extractMFSubStatus(type01),isInvert);					 
					 prsentScore=prsentScore00+prsentScore01;
					}
				}	
//			{
//				int backResult=back();
//				 prsentScore00=mFSubStatusList00.getScore(mf00.extractMFSubStatus(type00),isInvert);
//				 prsentScore01=mFSubStatusList01.getScore(mf01.extractMFSubStatus(type01),isInvert);					 
//				 prsentScore=prsentScore00+prsentScore01;
//				 if(backResult==-1) return -1;
//			}
		}
//		long endTimeA=System.currentTimeMillis();
		mFTwoSubStatusSolver.setInvert(isInvert);
		mFTwoSubStatusSolver.initTwoMFs(mf00.extractMFSubStatus(type00),mf01.extractMFSubStatus(type01));

		if(isDetail){
			twoSubStatusResult=mFTwoSubStatusSolver.solve();
		}else{
			twoSubStatusScore=mFTwoSubStatusSolver.getScore();
		}
//		long endTimeB=System.currentTimeMillis();
//		System.out.println("A:"+(startTimeA-endTimeA));
//		System.out.println("B:"+(endTimeA-endTimeB));
		return 1;
	}
	
	public void go(int nextStep)
	{
		mf00.move(nextStep);
		mf01.move(nextStep);
		Step _s=new Step((byte)nextStep,0);
		steps.push(_s);	
		addTwoSubStatus(mf00,mf01);
	}
	
	public int back()
	{
		Step _s=steps.pop();
		int stopMove=_s.getStepMove();
		if(stopMove==18) return -1;
		removeTwoSubStatus(mf00,mf01);
		mf00.remove(stopMove);
		mf01.remove(stopMove);
//		System.out.println("回退"+stopMove+",已走步数："+this.steps.size());
		steps.peek().setStopBit(stopMove);
		return 1;
	}
//构造器
	public MFWriteFaceSolver() {
		steps=new Stack<Step>();
		Step _s=new Step((byte)18,0);
		steps.push(_s);
		trace=new HashSet<TwoSubStatus>();
		this.isInvert=false;
		this.type00=TYPE00;
		this.type01=TYPE01;
		this.isDetail=true;
	}
	
	private void initStepScores(List<StepScore> stepScores)
	{
		for(int i1=0;i1<=17;i1++)
			stepScores.add(new StepScore());
	}
	
	public int findScoreDepth(int childDepthRestricted,int[] betterStep)
	{
		if(childDepthRestricted==0)
		{
			betterStep[0]=18;
			return 100;			
		}

		 int[] score00=new int[18];
		 int[] score01=new int[18];
		 List<StepScore> stepScores=new ArrayList<StepScore>(18);
		 initStepScores(stepScores);
		 

		int presentScore00=mFSubStatusList00.getScore(mf00.extractMFSubStatus(type00),isInvert);	
		int presentScore01=mFSubStatusList01.getScore(mf01.extractMFSubStatus(type01),isInvert);	
		int presentTotalScore=presentScore00+presentScore01;
		for(int i1=0;i1<=17;i1++)
		{
			mf00.move(i1);
			mf01.move(i1);
			score00[i1]=mFSubStatusList00.getScore(mf00.extractMFSubStatus(type00),isInvert);	
			score01[i1]=mFSubStatusList01.getScore(mf01.extractMFSubStatus(type01),isInvert);	
			stepScores.get(i1).setStep(i1);
			stepScores.get(i1).setScore(score00[i1]+score01[i1]);
			mf00.remove(i1);
			mf01.remove(i1);
		}
		Collections.sort(stepScores, stepScoreComparator);
		
		int minDepth=100;int nextStep=18;
		boolean hasChild=false;
		for(int i1=0;i1<=17;i1++)
		{
			int step=stepScores.get(i1).getStep();
			int score=stepScores.get(i1).getScore();
			if(score<presentTotalScore)
			{
				betterStep[0]=step;
				return 1;				
			}
			else if(score==presentTotalScore)
			{
				mf00.move(step);
				mf01.move(step);
				int[] betterGrandChildStep=new int[1];
				int childDepth=findScoreDepth(childDepthRestricted-1,betterGrandChildStep);
				mf00.remove(step);
				mf01.remove(step);
				if(!hasChild)
					{minDepth=childDepth;
					nextStep=step;
					hasChild=true;
					}
				else
				{
					minDepth=(minDepth<childDepth)?minDepth:childDepth;
					nextStep=(minDepth<childDepth)?nextStep:step;
				}
			}
			else break;
		}
		betterStep[0]=(minDepth>=100)?nextStep:18;
		return minDepth+1;
		
	}
	

    private	Comparator<StepScore> stepScoreComparator=new
			Comparator<StepScore>()
			{
		public int compare(StepScore a,StepScore b)
		{
			return a.getScore()-b.getScore();
		}
		};
	
		private TwoSubStatus extractTwoSubStatus(MFCode otherMf00,MFCode otherMf01)
		{
			int subStatus00=otherMf00.extractMFSubStatus(type00);
			int subStatus01=otherMf01.extractMFSubStatus(type01);
			return new TwoSubStatus(subStatus00,subStatus01);
		}
		
		private boolean hasTwoSubStatus(MFCode otherMf00,MFCode otherMf01)
		{
			TwoSubStatus twoSubStatus=extractTwoSubStatus(otherMf00,otherMf01);
			return trace.contains(twoSubStatus);
		}
		
		private void addTwoSubStatus(MFCode otherMf00,MFCode otherMf01)
		{
			TwoSubStatus twoSubStatus=extractTwoSubStatus(otherMf00,otherMf01);
			trace.add(twoSubStatus);
		}
		
		private void removeTwoSubStatus(MFCode otherMf00,MFCode otherMf01)
		{
			TwoSubStatus twoSubStatus=extractTwoSubStatus(otherMf00,otherMf01);
			trace.remove(twoSubStatus);
		}
		
		public void arrangeStep(String[] outStr)
		{
			if(!isDetail) return;
			int beg=0;
			int end=0;
			boolean lastOne=false;
			Step _step=null;
			twoSubStatusResult+=";";
			while(twoSubStatusResult.charAt(beg)!=';'){
				 end=twoSubStatusResult.indexOf(',', beg);
				 if(end==-1){
					 lastOne=true;
					 end=twoSubStatusResult.indexOf(';', beg);
				 }
				_step=new Step(Byte.valueOf(twoSubStatusResult.substring(beg, end)),0);
				steps.push(_step);
				beg=end+1;
				if(lastOne) break;
			}
			
			String str="";
//			System.out.println(steps.size()-1);
			if (this.steps.size() > 1) {

			for(int i1=1;i1<steps.size();i1++){
				str+=steps.elementAt(i1).getStepMove()+",";
			}
			
//			System.out.println(str);
		}
			outStr[0]=str;
		}
		
		public void arrangeStep(){
			String[] outStr={""};
			arrangeStep(outStr);
		}
		
		public int getScore(){
			return steps.size()-1+twoSubStatusScore;
		}
		
		public void reset(){
			steps.clear();
			Step _s=new Step((byte)18,0);
			steps.push(_s);
			trace.clear();
			isDetail=true;
			twoSubStatusScore=0;
		}
		
		public int getYellowFaceScore(int subStatus03,int subStatus04,String order){
			//mFWriteFaceSolver YFace On
			int yScore=99;
			setInvert(true);
			setDetail(false);
			if(order!=null)
			setOriginalPosition(subStatus03,subStatus04,order);
			else
				setOriginalPosition(subStatus03,subStatus04);
//			mFWriteFaceSolver.setOriginalPosition(cols,null);
			String[] outStr={""};
			if(solver(true)!=-1){
				arrangeStep(outStr);
				yScore=getScore();
//				System.out.println("YS:"+yScore);
			}
			reset();
			return yScore;
	//mFWriteFaceSolver YFace On
		}

	public static void main(String[] args)
	{
//		MFWriteFaceSolver类测试代码1
		Resource rs= new ClassPathResource("appcontext.xml");
		BeanFactory factory=new XmlBeanFactory(rs);
		MFWriteFaceSolver mFWriteFaceSolver=(MFWriteFaceSolver)factory.getBean("MFWriteFaceSolver");
		
		String[] cols = { 
		"bowgworww",
		"owyoyywry",
		"ggybgwgrr",
		"bywbbroyg",
		"rrborwogr", 
		"ybogoygbb",
};	
//		List<Integer> moveOrders=null;
//String moveOrder="2,10,7,10,14,5,1,13,15,1,15,4,0,16,1,8,16,8,";
//String[] _moveOrders=moveOrder.split(",");
//moveOrders=new ArrayList<Integer>();
//for(int i1=0;i1<_moveOrders.length;i1++){
//	moveOrders.add(Integer.valueOf(_moveOrders[i1]));
//}
		
//		Random random=new Random(System.currentTimeMillis());
//		System.out.println(random.nextInt());
//		System.out.println(random.nextInt());
		MFCode mf=(MFCode)factory.getBean("MFCodeImpl");
		mf.parseMFArray(cols);
//mf.move("2,13,4,2,15,5,2,0,4,15,7,13,7,1,14,16,1,14,17,0,14,16,");
//mf.showMF();
		long startTime;
		long endTime;
//				
//		System.out.println(mf.extractMFSubStatus("00"));
//		System.out.println(mf.extractMFSubStatus("01"));
//		System.out.println(mf.extractMFSubStatus("03"));
//		System.out.println(mf.extractMFSubStatus("04"));
		
////mFWriteFaceSolver On
//		mFWriteFaceSolver.setInvert(true);
//		mFWriteFaceSolver.setDetail(false);
//		mFWriteFaceSolver.setOriginalPosition(mf.extractMFSubStatus("03", false),mf.extractMFSubStatus("04", false));
////		 startTime=System.currentTimeMillis();
////		mFWriteFaceSolver.setOriginalPosition(cols,moveOrders);		
////		mFWriteFaceSolver.setOriginalPosition(cols,null);
//		String[] outStr={""};
//		if(mFWriteFaceSolver.solver(true)!=-1){
//			mFWriteFaceSolver.arrangeStep(outStr);
////			System.out.println(outStr[0]);
//			System.out.println(mFWriteFaceSolver.getScore());
//		}
////mFWriteFaceSolver OFF
		System.out.println(mFWriteFaceSolver.getYellowFaceScore(mf.extractMFSubStatus("03"), mf.extractMFSubStatus("04"), null));
		System.out.println(mFWriteFaceSolver.getYellowFaceScore(mf.extractMFSubStatus("03"), mf.extractMFSubStatus("04"), null));

////		 endTime=System.currentTimeMillis();
//		System.out.println(mFWriteFaceSolver.getScore());

//		mf.move(0);mf.move(6);mf.move(14);mf.move(16);
//		mf.move(8);mf.move(4);mf.move(6);

//		
//		mf.showMF();
		

//for(int i1=0;i1<moveOrders.size();i1++){
//	mf.move(moveOrders.get(i1));
//}
//mf.showMF();
//
	}
	
	
//	最后得分:20,9,11
//	最后得分:20
//	2,10,7,10,14,5,1,

//	4,8,16,0,10,10,6,15,7,
	
//	13,15,1,15,4,0,16,1,8,16,8,
	
}


//mFWriteFaceSolver 白 On
//		mFWriteFaceSolver.setInvert(false);
//		mFWriteFaceSolver.setDetail(true);
//		mFWriteFaceSolver.setOriginalPosition(mf.extractMFSubStatus("00", false),mf.extractMFSubStatus("01", false));
//		String[] outStr={""};
//		if(mFWriteFaceSolver.solver(true)!=-1){
//			mFWriteFaceSolver.arrangeStep(outStr);
//			System.out.println(outStr[0]);
//		}
//mFWriteFaceSolver 白 OFF


//mFWriteFaceSolver 黄 On
//mFWriteFaceSolver.setInvert(true);
//mFWriteFaceSolver.setDetail(true);
//mFWriteFaceSolver.setOriginalPosition(mf.extractMFSubStatus("03", false),mf.extractMFSubStatus("04", false));
//String[] outStr={""};
//if(mFWriteFaceSolver.solver(true)!=-1){
//	mFWriteFaceSolver.arrangeStep(outStr);
//	System.out.println(outStr[0]);
//}
//mFWriteFaceSolver 黄 OFF
