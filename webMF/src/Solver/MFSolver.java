package Solver;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import MFCode.MFCode;

public class MFSolver {
public MFWriteFaceSolver getmFWriteFaceSolver() {
		return mFWriteFaceSolver;
	}

	public void setmFWriteFaceSolver(MFWriteFaceSolver mFWriteFaceSolver) {
		this.mFWriteFaceSolver = mFWriteFaceSolver;
	}

	public MFTwoFaceSolver2 getmFTwoFaceSolver2() {
		return mFTwoFaceSolver2;
	}

	public void setmFTwoFaceSolver2(MFTwoFaceSolver2 mFTwoFaceSolver2) {
		this.mFTwoFaceSolver2 = mFTwoFaceSolver2;
	}

	public MFMidSolver getmFMidSolver() {
		return mFMidSolver;
	}

	public void setmFMidSolver(MFMidSolver mFMidSolver) {
		this.mFMidSolver = mFMidSolver;
	}

private MFWriteFaceSolver mFWriteFaceSolver;
private MFTwoFaceSolver2 mFTwoFaceSolver2;
private MFMidSolver mFMidSolver;
private MFCode mf;

public boolean solver(String[] cols,List<String> orders) throws SQLException{
	mf.parseMFArray(cols);
	
	//mFWriteFaceSolver On		
	mFWriteFaceSolver.setInvert(false);
	mFWriteFaceSolver.setDetail(true);
	mFWriteFaceSolver.setOriginalPosition(mf.extractMFSubStatus("00"),mf.extractMFSubStatus("01"));		
	String[] outStr={""};
	if(mFWriteFaceSolver.solver(true)!=-1){
		mFWriteFaceSolver.arrangeStep(outStr);
	}
	else {
		mFWriteFaceSolver.reset();
		return false;
	}
	mFWriteFaceSolver.reset();
	//mFWriteFaceSolver OFF
	mf.move(outStr[0]);
	
	
	String[] recoverOrder0={""};
	String[] recoverOrder1={""};
	
	if(!mFTwoFaceSolver2.solver(mf, recoverOrder0, recoverOrder1)){
		return false;
	}
	mf.move(recoverOrder0[0]);
	mf.move(recoverOrder1[0]);
	String lastOrder=mFMidSolver.queryOrder(mf.extractMFSubStatus("02")+"");
	if(lastOrder==null) return false;
	orders.clear();
	orders.add(outStr[0]);
	orders.add(recoverOrder0[0]);
	orders.add(recoverOrder1[0]);
	orders.add(lastOrder);
	return true;
}

		public MFCode getMf() {
	return mf;
}

public void setMf(MFCode mf) {
	this.mf = mf;
}

//		public static void main(String[] args){
//			Resource rs= new ClassPathResource("appcontext.xml");
//			BeanFactory factory=new XmlBeanFactory(rs);	
//			
//			MFCode mf=(MFCode)factory.getBean("MFCodeImpl");
//			String[] cols={
//					"wwwwwwwww",
//					"yyyyyyyyy",
//					"ggggggggg",
//					"bbbbbbbbb",
//					"rrrrrrrrr",
//					"ooooooooo"
//			};
//			mf.parseMFArray(cols);
//			String behave="7,16,4,17,9,5,10,3,7,15,6,3,10,16,6,9,13,3,12,4,5,2,13,6,3,12,0,7,16,0,,3,3,16,4,11,16,9,15,9,12,3,13,15,";
//			mf.move(behave);
//			mf.showMF();
//		}

public static void main(String[] args) throws SQLException{
	Resource rs= new ClassPathResource("appcontext.xml");
	BeanFactory factory=new XmlBeanFactory(rs);	
	String[] cols = { 
			"oggrwyygw",
			"rrwwyrybb",
			"wyybggboo",
			"gbgbbgwwb",
			"rygwroyoo", 
			"ryrrowoob",
			};
	MFSolver mFSolver=(MFSolver)factory.getBean("MFSolver");
	MFCode mf=(MFCode)factory.getBean("MFCodeImpl");
	mf.parseMFArray(cols);
	List<String> orders=new ArrayList<String>();
	if(mFSolver.solver(cols, orders)){
		for(int i1=0;i1<orders.size();i1++){
			System.out.println(orders.get(i1));
			mf.move(orders.get(i1));
		}
	}
	mf.showMF();
}
}

