package Solver;

public class Step
{
	private byte stepMove;
	private int stopBits;
	public void setStepMove(byte stepMove)
	{
		this.stepMove=stepMove;
	}
	public byte getStepMove()
	{
		return stepMove;
	}
	
	
	public void setStopBit(int stopStep)
	{
		int _a=1;
		stopBits|=_a<<stopStep;
	}	
	public boolean isStopStep(int stopStep)
	{
		int _a=1;
		int _result=(_a<<stopStep)&stopBits;
		return (_result==0)?false:true;
	}
	
	public Step(byte stepMove,int stopBits)
	{
		this.stepMove=stepMove;
		this.stopBits=stopBits;
	}	
}
