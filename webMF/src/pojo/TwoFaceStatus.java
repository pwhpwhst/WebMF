package pojo;

public class TwoFaceStatus implements Comparable {
	private int subStatus00;
	private int subStatus01;
	private int subStatus03;
	private int subStatus04;
	
	public void setTwoSubStatus(int subStatus00,int subStatus01,int subStatus03,int subStatus04)
	{
		this.subStatus00=subStatus00;
		this.subStatus01=subStatus01;
		this.subStatus03=subStatus03;
		this.subStatus04=subStatus04;
	}
	
	public int getSubStatus00() {
		return subStatus00;
	}

	public int getSubStatus01() {
		return subStatus01;
	}
	
	public int getSubStatus03() {
		return subStatus03;
	}

	public int getSubStatus04() {
		return subStatus04;
	}

	public TwoFaceStatus(int subStatus00,int subStatus01,int subStatus03,int subStatus04)
	{
		this.subStatus00=subStatus00;
		this.subStatus01=subStatus01;
		this.subStatus03=subStatus03;
		this.subStatus04=subStatus04;
	}
	
	public int hashCode()
	{
		return subStatus00;
	}
	
	public boolean equals(Object obj)
	{
		TwoFaceStatus obj1=(TwoFaceStatus)obj;
		if((obj1.getSubStatus00()==subStatus00)&&(obj1.getSubStatus01()==subStatus01)
				&&(obj1.getSubStatus03()==subStatus03)&&(obj1.getSubStatus04()==subStatus04))
			return true;
		return false;
	}
	
	@Override
	public int compareTo(Object arg0) {

		int[] a={0,0,0,0};
		
		int num=((TwoFaceStatus)arg0).getSubStatus00();
		if(subStatus00!=num)
		 a[0]=subStatus00<num?-1:1;
		
		 num=((TwoFaceStatus)arg0).getSubStatus01(); 
		 if(subStatus01!=num)
		 a[1]=subStatus01<num?-1:1;
		 
		 num=((TwoFaceStatus)arg0).getSubStatus03();
		 if(subStatus03!=num)
		 a[2]=subStatus03<num?-1:1;
		 
		 num=((TwoFaceStatus)arg0).getSubStatus04();
		 if(subStatus04!=num)
		 a[3]=subStatus04<num?-1:1;
			
		 
		 int i1=0;
		 while(i1<4&&a[i1]==0){
			 i1++;
		 }
		return (i1==4)?0:a[i1];
	}
}
