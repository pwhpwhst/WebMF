package pojo;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import Comparator.MfStatisticForScoreChangeComparator;

public class MFStaticForScoreChangMain {
	private int score;
	private int total;
	private String scoreChangeStatic;
	private List<MfStatisticForScoreChange> list;
	private Comparator<MfStatisticForScoreChange> comparator;
	
	public Comparator<MfStatisticForScoreChange> getComparator() {
		if(comparator==null){
			Resource rs= new ClassPathResource("appcontext.xml");
			BeanFactory factory=new XmlBeanFactory(rs);	
			MfStatisticForScoreChangeComparator u=(MfStatisticForScoreChangeComparator)factory.getBean("MfStatisticForScoreChangeComparator");
			setComparator(u);
		}
		return comparator;
	}

	public void setComparator(Comparator<MfStatisticForScoreChange> comparator) {
		this.comparator = comparator;
	}

	public void scoreChangeStaticToList(){
		if(scoreChangeStatic==null)
			return;
		list=new ArrayList<MfStatisticForScoreChange>();
		
		int _beg=0;
		int _end;
		while((_end=scoreChangeStatic.indexOf(";", _beg))>0){
			int _end2=scoreChangeStatic.indexOf(":", _beg);
			int score=Integer.valueOf(scoreChangeStatic.substring(_beg, _end2));
			int freq=Integer.valueOf(scoreChangeStatic.substring(_end2+1, _end));
			MfStatisticForScoreChange u=new MfStatisticForScoreChange();
			u.setScore(score);
			u.setFreq(freq);
			list.add(u);
			_beg=_end+1;
		}
	}
	
	public void listToScoreChangeStatic(){
		StringBuffer str=new StringBuffer();
		if(list==null) return;
		
		Collections.sort(list, getComparator());
		for(int i1=0;i1<list.size();i1++){
			MfStatisticForScoreChange u=list.get(i1);
			str.append(u.getScore()+":"+u.getFreq()+";");
		}
		scoreChangeStatic=str.toString();
	}
	
	public int getMfStatisticForScoreChange(int scoreDifference){
		MfStatisticForScoreChange _u=new MfStatisticForScoreChange();
		_u.setScore(scoreDifference);
		if(list==null)
			list=new ArrayList<MfStatisticForScoreChange>();
		int index=list.indexOf(_u);
		if(index<0){
			_u.setFreq(0);
			list.add(_u);
			return 0;
		}else
		return list.get(index).getFreq();
	}
	
	public void setMfStatisticForScoreChange(int scoreDifference,int freq){
		MfStatisticForScoreChange _u=new MfStatisticForScoreChange();
		_u.setScore(scoreDifference);
		if(list==null)
			list=new ArrayList<MfStatisticForScoreChange>();
		int index=list.indexOf(_u);	
		if(index<0){
			_u.setFreq(freq);
			list.add(_u);
		}else
		list.get(index).setFreq(freq);
	}
	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	public int getTotal() {
		return total;
	}

	public void setTotal(int total) {
		this.total = total;
	}

	public String getScoreChangeStatic() {
		return scoreChangeStatic;
	}

	public void setScoreChangeStatic(String scoreChangeStatic) {
		this.scoreChangeStatic = scoreChangeStatic;
		scoreChangeStaticToList();
	}

	public static void main(String[] arg){
		MFStaticForScoreChangMain mFStaticForScoreChangMain=new MFStaticForScoreChangMain();
		mFStaticForScoreChangMain.setMfStatisticForScoreChange(0, -1);
		mFStaticForScoreChangMain.setMfStatisticForScoreChange(4, 8);
		int a=mFStaticForScoreChangMain.getMfStatisticForScoreChange(0);
		System.out.println(a);
		mFStaticForScoreChangMain.listToScoreChangeStatic();
		System.out.println(mFStaticForScoreChangMain.getScoreChangeStatic());
	}
	
	
}
