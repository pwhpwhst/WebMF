package pojo;

import java.util.HashSet;
import java.util.Set;

public class TwoSubStatus implements Comparable
{
	private int subStatus00;
	private int subStatus01;
	
	public void setTwoSubStatus(int subStatus00,int subStatus01)
	{
		this.subStatus00=subStatus00;
		this.subStatus01=subStatus01;
	}
	
	public int getSubStatus00() {
		return subStatus00;
	}

	public int getSubStatus01() {
		return subStatus01;
	}

	public TwoSubStatus(int subStatus00,int subStatus01)
	{
		this.subStatus00=subStatus00;
		this.subStatus01=subStatus01;
	}
	
	public int hashCode()
	{
		return subStatus00;
	}
	
	public boolean equals(Object obj)
	{
		TwoSubStatus obj1=(TwoSubStatus)obj;
		if((obj1.getSubStatus00()==subStatus00)&&(obj1.getSubStatus01()==subStatus01))
			return true;
		return false;
	}
	
	public static void main(String[] arg)
	{
		Set<TwoSubStatus> _set=new HashSet<TwoSubStatus>();
		TwoSubStatus a=new TwoSubStatus(0,1);
		TwoSubStatus b=new TwoSubStatus(0,1);
		_set.add(a);
		_set.remove(a);
		System.out.println(_set.contains(b));
	}

	@Override
	public int compareTo(Object arg0) {
		int num=((TwoSubStatus)arg0).getSubStatus00();
		int result=0;
		if(subStatus00==num)
		{
			num=((TwoSubStatus)arg0).getSubStatus01();
			if(subStatus01==num)
				result=0;
			else
				result=subStatus01<num?-1:1;
		}
		else
		{
			result=subStatus00<num?-1:1;
		}
			
		return result;
	}
}
