package pojo;

public class MFSubStatus{
	
	private int subStatus;
	private int score;
	private int parent;
	private int littlerBrother;
	private int firstChild;
	private int isVisited;
	private int depth;
	private int parentSubstatus;
	
	public MFSubStatus(){
		littlerBrother=18;
		firstChild=18;
		parent=18;
		isVisited=0;
		depth=1;
	}
	
	public int getSubStatus() {
		return subStatus;
	}
	public void setSubStatus(int subStatus) {
		this.subStatus = subStatus;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public int getParent() {
		return parent;
	}
	public void setParent(int parent) {
		this.parent = parent;
	}
	public int getLittlerBrother() {
		return littlerBrother;
	}
	public void setLittlerBrother(int littlerBrother) {
		this.littlerBrother = littlerBrother;
	}
	public int getFirstChild() {
		return firstChild;
	}
	public void setFirstChild(int firstChild) {
		this.firstChild = firstChild;
	}
	public int getIsVisited() {
		return isVisited;
	}
	public void setIsVisited(int isVisited) {
		this.isVisited = isVisited;
	}
	public int getDepth() {
		return depth;
	}
	public void setDepth(int depth) {
		this.depth = depth;
	}
	public int getParentSubstatus() {
		return parentSubstatus;
	}
	public void setParentSubstatus(int parentSubstatus) {
		this.parentSubstatus = parentSubstatus;
	}

}