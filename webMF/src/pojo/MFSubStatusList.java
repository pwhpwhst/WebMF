package pojo;

import java.sql.SQLException;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.springframework.beans.factory.BeanFactory;
import org.springframework.beans.factory.xml.XmlBeanFactory;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;

import Dao.CubeDao;

import com.ibatis.sqlmap.client.SqlMapClient;

public class MFSubStatusList{
	private String type;
	private String MFTable;
	private List<MFSubStatusParent> list=null;
//	private  SqlMapClient sqlMap=null;
	private CubeDao cubeDao;
	private Comparator<MFSubStatusParent> comparator=null;
	
	public List<MFSubStatusParent> getList() {
	return list;
}
	public String getType() {
	return type;
}
	
	public MFSubStatusList(String type,CubeDao cubeDao,Comparator<MFSubStatusParent> comparator)
	{
		this.type=type;
		this.cubeDao=cubeDao;
		this.comparator=comparator;
		
		if(type.equals("00"))
			MFTable="MF_SUBSTATUS00_SCORE";
		else if(type.equals("01"))
			MFTable="MF_SUBSTATUS01_SCORE";

		getMFSubStatusParentList();
	}
	
	public int getScore(int subStatus,boolean isInverse){
		MFSubStatusParent mFSubStatusParent=new MFSubStatusParent();
		if(isInverse){
			subStatus=invertSubstatus(subStatus);
		}
		mFSubStatusParent.setSubStatus(subStatus);
		int index=Collections.binarySearch(list, mFSubStatusParent, comparator);
		return list.get(index).getScore();
	}
	
		private void getMFSubStatusParentList()
		{
			System.out.println("开始创建MFSubStatusList"+getType()+"...");
			long startTime=System.currentTimeMillis();
			list=(List<MFSubStatusParent>)cubeDao.queryForList(MFTable+"_GET_MFSUBSTATUS_PARENTLIST");
			long endTime=System.currentTimeMillis();
			System.out.println("创建结束,耗时"+(endTime-startTime)/1000+"秒");
		}
		

		
		public int invertSubstatus(int subStatus){
			int[] _convert={0,3,2,1};
			int[] _convert2={255,65280,16711680,2130706432};
			int[] _convert3={3,12,48,192};
			int[] _order={0,1,2,3};
			boolean[] _forword={true,false,true,false};
//			int subStatus=-2147483648;
//			int u1=(subStatus>0)?subStatus:-(subStatus+1);
			int u1=(subStatus>0)?subStatus:subStatus+1073741824+1073741824;
			int newU1=0;
			int u2=0;
			int newU2=0;
			for(int i1=0;i1<=3;i1++){
				 u2=(u1&_convert2[i1])>>(i1*8);
					newU2=0;
					boolean isZAxisEqualsZero=false;
					boolean isTwoGridType=false;
					int _x=0;
				for(int i2=0;i2<=2;i2++){
					if(i2!=0){
						_x=_convert[(u2&_convert3[i2])>>(i2*2)]<<(i2*2);
						isTwoGridType=(_x==0)?true:isTwoGridType;
						newU2|=_x;

					}else{
						 _x=u2&_convert3[i2];
						 isTwoGridType=(_x==0)?true:isTwoGridType;
						 isZAxisEqualsZero=(_x==0)?true:false;
						newU2|=_x;//Z轴的数据不需要转化
					}
				}
				if(isTwoGridType)
				newU2|=(isZAxisEqualsZero^_forword[i1])?(u2&(192))^64:u2&(192);
				else
					newU2|=u2&(192);
				newU1|=newU2<<(_order[i1]*8);
			}
			return (subStatus>0)?newU1:newU1-1073741824-1073741824;	
			}
		
		
		public static void main(String[] args){

		}
	}
