package pojo;

import java.math.BigDecimal;
import java.util.Comparator;

public class MFSubStatusParent 
{
	private int subStatus;
	private int parentMove;
	private int parentSubStatus;
	private int score;
	
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public int getSubStatus() {
		return subStatus;
	}
	public void setSubStatus(int subStatus) {	
		this.subStatus = subStatus;
	}
	

	public int getParentMove() {
		return parentMove;
	}
	public void setParentMove(int parentMove) {
		this.parentMove = parentMove;
	}
	
	public int getParentSubStatus() {
		return parentSubStatus;
	}
	public void setParentSubStatus(int parentSubStatus) {
		this.parentSubStatus = parentSubStatus;
	}
	
	
//	public void setParentSubStatus(int parentSubStatus) {
//		this.parentSubStatus = parentSubStatus;
//	}

	
	
	public MFSubStatusParent()
	{
		  subStatus=0;
		  parentMove=0;
		  parentSubStatus=0;
	}


}