package pojo;

public class MfStatisticForScoreChange implements Comparable{
	private int score;
	private int freq;
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public int getFreq() {
		return freq;
	}
	public void setFreq(int freq) {
		this.freq = freq;
	}
	
	@Override
	public int compareTo(Object o) {
		if( score==((MfStatisticForScoreChange)o).getScore())
			return 0;
		return score<((MfStatisticForScoreChange)o).getScore()?-1:1;
	}
	
	public int hashCode()
	{
		return score;
	}
	
	public boolean equals(Object obj)
	{
		if(((MfStatisticForScoreChange)obj).getScore()==score)
			return true;
		return false;
	} 
	
}
