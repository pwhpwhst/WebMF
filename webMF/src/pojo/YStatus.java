package pojo;

public class YStatus {
	public String substatus;
	public int score;
	public String nextRecoverOrder;
	public String otherInfo;

	public String getOtherInfo() {
		return otherInfo;
	}
	public void setOtherInfo(String otherInfo) {
		this.otherInfo = otherInfo;
	}
	public String getSubstatus() {
		return substatus;
	}
	public void setSubstatus(String substatus) {
		this.substatus = substatus;
	}
	public int getScore() {
		return score;
	}
	public void setScore(int score) {
		this.score = score;
	}
	public String getNextRecoverOrder() {
		return nextRecoverOrder;
	}
	public void setNextRecoverOrder(String nextRecoverOrder) {
		this.nextRecoverOrder = nextRecoverOrder;
	}
	
	public int hashCode(){
		return 0;
	}
	
	public boolean equals(Object obj){
		return substatus.equals(((YStatus)obj).getSubstatus());
	}
	
}
