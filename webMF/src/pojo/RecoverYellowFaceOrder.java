package pojo;

public class RecoverYellowFaceOrder {
public String recoverOrder;
public Integer length;

public Integer getLength() {
	return length;
}
public void setLength(Integer length) {
	this.length = length;
}
public String getRecoverOrder() {
	return recoverOrder;
}
public void setRecoverOrder(String recoverOrder) {
	this.recoverOrder = recoverOrder;
}

public int yscore;

public int getYscore() {
	return yscore;
}
public void setYscore(int yscore) {
	this.yscore = yscore;
}
public int calculateOrderLength(){
	if(recoverOrder!=null)
	return recoverOrder.split(",").length;
	else return 0;
}
}
