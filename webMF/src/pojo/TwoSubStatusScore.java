package pojo;

import java.util.HashSet;
import java.util.Set;

public class TwoSubStatusScore implements Comparable
{
	private int subStatus00;
	private int subStatus01;
	private int score;
	private String nextStep;
	

	
	public String getNextStep() {
		return nextStep;
	}

	public void setNextStep(String nextStep) {
		this.nextStep = nextStep;
	}

	public void setSubStatus00(int subStatus00) {
		this.subStatus00 = subStatus00;
	}

	public void setSubStatus01(int subStatus01) {
		this.subStatus01 = subStatus01;
	}

	public int getSubStatus00() {
		return subStatus00;
	}

	public int getSubStatus01() {
		return subStatus01;
	}

	public TwoSubStatusScore()
	{
	}
	
	public int hashCode()
	{
		return subStatus00;
	}
	
	public boolean equals(Object obj)
	{
		TwoSubStatusScore obj1=(TwoSubStatusScore)obj;
		if((obj1.getSubStatus00()==subStatus00)&&(obj1.getSubStatus01()==subStatus01))
			return true;
		return false;
	}
	
//	public static void main(String[] arg)
//	{
//		Set<TwoSubStatusScore> _set=new HashSet<TwoSubStatusScore>();
//		TwoSubStatusScore a=new TwoSubStatusScore(0,1);
//		TwoSubStatusScore b=new TwoSubStatusScore(0,1);
//		_set.add(a);
//		_set.remove(a);
//		System.out.println(_set.contains(b));
//	}

	public int getScore() {
		return score;
	}

	public void setScore(int score) {
		this.score = score;
	}

	@Override
	public int compareTo(Object arg0) {
		int num=((TwoSubStatusScore)arg0).getSubStatus00();
		int result=0;
		if(subStatus00==num)
		{
			num=((TwoSubStatusScore)arg0).getSubStatus01();
			if(subStatus01==num)
				result=0;
			else
				result=subStatus01<num?-1:1;
		}
		else
		{
			result=subStatus00<num?-1:1;
		}
			
		return result;
	}
}
