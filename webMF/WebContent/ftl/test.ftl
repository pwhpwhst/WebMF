<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<META HTTP-EQUIV="Pragma" CONTENT="no-cache">
<META HTTP-EQUIV="Cache-Control" CONTENT="no-cache">
<META HTTP-EQUIV="Expires" CONTENT="0">
<title>cube操作页面</title>
<link rel="stylesheet" type="text/css" href="/webMF/styles/project.css"></link>
<script charset="utf-8" src="/webMF/scripts/jquery-1.4.2.min.js"></script>
</head>
<body>
	<table>
		<tr>
			<td>
				<iframe id="cubeFrame" src="CubeAction_test2.action" scrolling=no frameborder=0 height=500 width=500></iframe>
			</td>
				<td>
					<table class="inner_table"  border="1">
						<tr class="inner_first_tr">
							<td>nextMove</td>
							<td>s00</td>
							<td>score</td>
							<td>s00</td>
							<td>score01</td>
							<td>s00</td>
							<td>score02</td>
							<td>s00</td>
							<td>score03</td>
							<td>s00</td>
							<td>score04</td>
						</tr>						
						<tr class="inner_tr">
							<td>nextMove</td>
							<td>1416891699</td>
							<td>score00</td>
							<td>1416891699</td>
							<td>score01</td>
							<td>1416891699</td>
							<td>score02</td>
							<td>1416891699</td>
							<td>score03</td>
							<td>1416891699</td>
							<td>score04</td>
						</tr>

					</table>

				</td>
		</tr>
		<tr>
			<td>
				<table>
					<tr>
						<td style="width:80px"></td>
							<td><#include "/ftl/cubeEdit.ftl"></td>
						<td style="width:10px"></td>
					</tr>
				</table>
			</td>
			<td>
			<table><tr>
			<td style="width:80px"></td>
			<td>
			<input class="btnmain" style="width:80px" type="button" value="生成指令" onclick="getRecoverOrder()"></input>
			<input class="btnmain" style="width:60px" type="button" value="执行" onclick="actOrder()"></input>
			</td></tr></table>
				<table>
					<tr>
						<td>
							<table>
								<tr><td><input type="radio" name="orderType" class="radio" onclick="beforeView();"/>观察</td></tr>
								<tr><td><input type="radio" name="orderType" class="radio" onclick="afterView();" checked/>破解</td></tr>
							</table>
						</td>
						<td>
							<textarea style="width:638px; height:138px;resize: none;"></textarea>
						</td>
					</tr>
	
				</table>
			</td>
		</tr>
	</table>
	<script>
var cubeReserve="";
var orderOfCubeReserve="";
var gridPosition=new Array();
var _gridPosition=new Array(0,1,2,3,4,5,6,7,8);//w
	gridPosition.push(_gridPosition);
	_gridPosition=new Array(2,11,20,5,14,23,8,17,26);//g
	gridPosition.push(_gridPosition);
	_gridPosition=new Array(20,19,18,23,22,21,26,25,24);//y
	gridPosition.push(_gridPosition);
	_gridPosition=new Array(18,9,0,21,12,3,24,15,6);//b
	gridPosition.push(_gridPosition);
	_gridPosition=new Array(18,19,20,9,10,11,0,1,2);//r
	gridPosition.push(_gridPosition);
	_gridPosition=new Array(6,7,8,15,16,17,24,25,26);//o
	gridPosition.push(_gridPosition);	

//将编辑区的颜色编辑函数，鼠标点击能改变颜色	
			function changeCol(){
			if(this.id=='W4'||this.id=='Y4'||this.id=='G4'||this.id=='B4'||this.id=='R4'||this.id=='O4')
				return;
				var cols=new Array('W','Y','G','B','R','O');
				var colsStyle=new Array('white',
				'yellow',
				'green',
				'blue',
				'red',
				'orange'
				);
				for(var i1=0;i1<cols.length;i1++){
					if(this.value==cols[i1]){
						this.value=cols[(i1+1)%6];
						this.style.backgroundColor=colsStyle[(i1+1)%6];
						break;
					}
				}
			}
			
			function addChangeColsFunc(){
				var cols=new Array('W','Y','G','B','R','O');
				for(var i1=0;i1<cols.length;i1++)
					for(var i2=0;i2<9;i2++){
						document.getElementById(cols[i1]+i2).onclick=changeCol;
					}
				}	
			document.getElementsByTagName('body')[0].onload=addChangeColsFunc;
			
			//从编辑区获取颜色矩阵
			function getColArray(){
			var colInputList=new Array('colInput0','colInput1');
			var returnCols=new Array();
			for(var i1=0;i1<colInputList.length;i1++){
					var colInputFaces=document.getElementById(colInputList[i1]).getElementsByTagName('table');
					for(var i2=0;i2<colInputFaces.length;i2++){
						var otr=colInputFaces[i2].getElementsByTagName('tr');
						var facecols='';
						for(var i3=0;i3<otr.length;i3++){
							var otd=otr[i3].getElementsByTagName('td');
							var rowcols='';
							for(var i4=0;i4<otr.length;i4++){
								rowcols+=otd[i4].getElementsByTagName('input')[0].value+',';
							}
							facecols+=rowcols;
						}
//						alert(facecols);
						returnCols.push(facecols);
					}
				}
				return returnCols;
			}
//document.getElementById('container').getElementsByClassName('cubeletId-0')[0].getElementsByClassName('faceFront')[0].getElementsByClassName('sticker')[0].setAttribute('class','sticker white')


//将编辑区的颜色画到魔方上
function drawCube(){
	colArray=getColArray();
	var faceDirection=new Array('faceFront','faceRight','faceBack','faceLeft','faceUp','faceDown');
	for(var i1=0;i1<colArray.length;i1++){
		var cols=colArray[i1].split(',');
		for(var i2=0;i2<9;i2++){
			var cubeletId=gridPosition[i1][i2];
			var _faceDirection=faceDirection[i1];
			var _domClassName="";

			if(cols[i2]=='W')
				_domClassName="sticker white";
			else if(cols[i2]=='Y')
				_domClassName="sticker yellow";
			else if(cols[i2]=='G')
				_domClassName="sticker green";				
			else if(cols[i2]=='B')
				_domClassName="sticker blue";				
			else if(cols[i2]=='R')
				_domClassName="sticker red";
			else if(cols[i2]=='O')
				_domClassName="sticker orange";
												
			var cubeDocument=document.getElementById('cubeFrame').contentWindow.document;
			cubeDocument.getElementById('container').getElementsByClassName('cubeletId-'+cubeletId)[0].getElementsByClassName(_faceDirection)[0].getElementsByClassName('sticker')[0].setAttribute('class',_domClassName);
		}
	}
}

//获取描述初始魔方的字符串数组
function getOrigionalCubeReserve(){
var faceDirection=new Array('faceFront','faceRight','faceBack','faceLeft','faceUp','faceDown');
		var colsForCube="";
		for(var i1=0;i1<6;i1++){
		var colsForFace="";
		for(var i2=0;i2<9;i2++){
			var cubeletId=gridPosition[i1][i2];
			var _faceDirection=faceDirection[i1];
			
			var cubeDocument=document.getElementById('cubeFrame').contentWindow.document;
			var _domClassName=cubeDocument.getElementById('container').getElementsByClassName('cubeletId-'+cubeletId)[0].getElementsByClassName(_faceDirection)[0].getElementsByClassName('sticker')[0].getAttribute('class');
			
			if(_domClassName=='sticker white')
				colsForFace+='w';
			else if(_domClassName=='sticker yellow')
				colsForFace+='y';
			else if(_domClassName=='sticker green')
				colsForFace+='g';				
			else if(_domClassName=='sticker blue')
				colsForFace+='b';				
			else if(_domClassName=='sticker red')
				colsForFace+='r';
			else if(_domClassName=='sticker orange')
				colsForFace+='o';
		}
		colsForFace+=";"
		colsForCube+=colsForFace;
}
return colsForCube;
}

function getHistoryOrder(){
	var orders="";
	var cubeWin=document.getElementById('cubeFrame').contentWindow;
	for(var i1=0;i1<cubeWin.cube.twistQueue.history.length;i1++){
		if(cubeWin.cube.twistQueue.history[i1]!=""){
			orders+=cubeWin.cube.twistQueue.history[i1].command+",";
		}
	}
	return orders.substring(0,orders.length-1);
}

function resetAndDrawCube(){
	var cubeWin=document.getElementById('cubeFrame').contentWindow;
		cubeWin.command=cubeWin.getCommand();
	cubeWin.resetCube();
	if(document.getElementsByName('orderType')[0].checked==true){
		cubeWin.command="";
	}
	setTimeout("drawCube();"+"var cubeWin=document.getElementById('cubeFrame').contentWindow;"+
	"cubeWin.startInteractiveCube();",3000);
}

function beforeView(){
	var cubeWin=document.getElementById('cubeFrame').contentWindow;
	cubeWin.beforeView();
}

function afterView(){
	var cubeWin=document.getElementById('cubeFrame').contentWindow;
	cubeWin.afterView();
}

function getRecoverOrder(){
	var ods=document.getElementsByName("orderType");
	var i1=0;
	for(i1=0;i1<ods.length;i1++){
		if(ods[i1].checked==true){
			break;
		}
	}
	if(ods[i1].innerHTML=="观察")
		return;
		changeAllInputStatus(true);
	var actionStr="CubeAction_getRecoverOrder";
	var colsForCube=getOrigionalCubeReserve();
	var historyOrder=getHistoryOrder();
	actionStr=actionStr+"?colsForCube="+colsForCube+"&historyOrder="+historyOrder;
	
	try{
		$.getJSON(actionStr,function(data){
		if(data=="404"){
			alert("魔方解析错误！");
			changeAllInputStatus(false);
		}else{
		alert("还原方法获取成功！共"+(data.split(",").length-4)+"步");
		document.getElementsByTagName("textarea")[0].innerHTML=data;	
		changeAllInputStatus(false);}})
		}catch(exception){
	alert("魔方解析错误！");
	changeAllInputStatus(false);
	}
}

function changeAllInputStatus(flag){
	var ods=document.getElementsByTagName("input");
	for(var i1=0;i1<ods.length;i1++){
		ods[i1].disabled=flag;
	}
}

function actOrder(){
var str=document.getElementsByTagName("textarea")[0].innerHTML;
document.getElementsByTagName("textarea")[0].innerHTML="";
var cubeWin=document.getElementById('cubeFrame').contentWindow;
cubeWin.cube.twist(str);
}

	</script>
</body>