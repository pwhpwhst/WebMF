var motion;
var startTime = 0;
var isMobile = dataStore['fpdoodle'] == '1';//定义于certificate.js文件
var scopedCheckQueue;
var ua = navigator.userAgent,
    isIe = ua.indexOf('MSIE') > -1 || ua.indexOf('Trident') > -1;
var useLockedControls = true,
    controls = useLockedControls ? ERNO.Locked : ERNO.Freeform;
//构造cube数据体 begin
window.cube = new ERNO.Cube({
  hideInvisibleFaces: isMobile,
  controls: controls, //与locked.js方法有关
  renderer: isIe ? ERNO.renderers.IeCSS3D : null
});
//构造cube数据体 end
cube.hide();
//将cube植入页面的'container'元素中 begin
var container = document.getElementById('container');
container.appendChild(cube.domElement);
//将cube植入页面的'container'元素中 end
if (isMobile) {
  document.body.classList.add('mobile');
  document.getElementById('bg').classList.add('graydient');
}
//添加cube的点击响应函数 begin
cube.addEventListener('click', function(evt) {
  if (!cube.mouseControlsEnabled) {
    return;
  }
  var cubelet = evt.detail.cubelet,
      face = cubelet[evt.detail.face.toLowerCase()],
      axis = new THREE.Vector3(),
      exclude = new THREE.Vector3(1, 0, 0),
      UP = new THREE.Vector3(0, 1, 0),
      normal = ERNO.Direction.getDirectionByName(face.normal).normal.clone(),
      slice;
  normal.x = Math.abs(normal.x);
  normal.y = Math.abs(normal.y);
  normal.z = Math.abs(normal.z);
  var l = cube.slices.length;
  while (l-- > 0) {
    slice = cube.slices[l];
    axis.copy(slice.axis);
    axis.x = Math.abs(axis.x);
    axis.y = Math.abs(axis.y);
    axis.z = Math.abs(axis.z);
    if (slice.cubelets.indexOf(cubelet) !== -1 &&
        axis.equals(UP)) {
      break;
    }
  }
  var command = slice.name.substring(0, 1);
  if (slice === cube.down) command = command.invert();
  cube.twist(command);
});
//添加cube的点击响应函数 end
/*
//设置cube和ERNO的相关参数 begin
if (controls === ERNO.Locked) {
  var fixedOrientation = new THREE.Euler(Math.PI * 0.1, Math.PI * -0.25, 0);
  cube.object3D.lookAt(cube.camera.position);
  cube.rotation.x += fixedOrientation.x;
  cube.rotation.y += fixedOrientation.y;
  cube.rotation.z += fixedOrientation.z;
}
cube.camera.position.z = 1600;
cube.camera.fov = 30;
cube.camera.updateProjectionMatrix();
if (isMobile) {
  cube.position.y = 0;
  cube.position.z = 0;
} else {
  cube.position.y = 410;
  cube.position.z = -850;
}
cube.mouseControlsEnabled = false;
cube.keyboardControlsEnabled = false;
cube.twistCountDown = 0;
cube.audioList = [];
cube.audio = 0;
ERNO.RED.hex = '#DC422F';
ERNO.WHITE.hex = '#FFF';
ERNO.BLUE.hex = '#3D81F6';
ERNO.GREEN.hex = '#009D54';
ERNO.ORANGE.hex = '#FF6C00';
ERNO.YELLOW.hex = '#FDCC09';
ERNO.COLORLESS.hex = '#000000';
//设置cube和ERNO的相关参数 end
*/
/*
// 定义魔方的阴影 begin
//定义 cube html句柄 begin
var Plane = function(cube, name, className) {
  THREE.Object3D.call(this);
  cube.object3D.add(this);
  this.domElement = document.createElement('div');
  this.domElement.classList.add(className);
  this.domElement.id = name;
  this.css3DObject = new THREE.CSS3DObject(this.domElement);
  this.css3DObject.name = 'css3DObject-' + name;
  this.add(this.css3DObject);
};
Plane.prototype = Object.create(THREE.Object3D.prototype);
//定义 cube html句柄 end
if (!isIe) {
  var shadow = new Plane(cube, 'shadow', 'shadow');
  shadow.rotation.set(
      (90).degreesToRadians(),
      (0).degreesToRadians(),
      (0).degreesToRadians()
  );
  shadow.position.y = -300;
  function updateShadow() {
    requestAnimationFrame(updateShadow);
    shadow.rotation.z = cube.slicesDictionary['y'].rotation;
  }
  requestAnimationFrame(updateShadow);
}
// 定义魔方的阴影 end
*/
//方法 setupLogo begin
window.setTimeout(setupLogo, 100);
function setupLogo() {
  cube.rotation.set(
      (25).degreesToRadians(),
      (-45).degreesToRadians(),
      (0).degreesToRadians()
  );
  cube.typeCubeletIds = new Array(8, 17, 16, 23, 20, 12, 21, 25);
//cube.typeCubeletIds = new Array(0,1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27);
  cube.typeCubelets = new ERNO.Group();
  
  cube.cubelets.forEach(function(cubelet, index) {
    cube.typeCubeletIds.forEach(function(id) {
      if (cubelet.id == id) {
        cube.typeCubelets.add(cubelet);
        cubelet.logo = true;
      }
    });
  }); 
  
  var stickerLogo = document.getElementsByClassName('stickerLogo');
  if (stickerLogo.length > 0) {
    stickerLogo[0].classList.remove('stickerLogo');
  }
  cube.twistDuration = 0;
//  var LOGO_SEQUENCE = 'zzxLFFRDuFLUrl'+'LRulfUdrfflXZZ';
var LOGO_SEQUENCE = '';
  cube.twistCountDown = LOGO_SEQUENCE.length;
  scopedCheckQueue = checkQueue.bind(this, startScrambleAnimation);
  cube.addEventListener('onTwistComplete', scopedCheckQueue);
  cube.twist(LOGO_SEQUENCE);
  // 设置魔方颜色的代码 begin
  /*
  function setWhiteBg(selector) {
    var elements = document.querySelectorAll(selector);
    for (var i = 0; i < elements.length; ++i) {
      elements[i].style.backgroundColor = ERNO.WHITE.hex;
    }
  }
  
  var prefix = '.cubeletId-';
  cube.cubelets.forEach(function(cubelet, index) {
    if (cubelet.logo != true) {
      setWhiteBg(prefix + cubelet.id + ' .sticker');
    }
    if (cubelet.id == 8 || cubelet.id == 17) {
      setWhiteBg(prefix + cubelet.id + ' .sticker.red');
    }
    if (cubelet.id == 21 || cubelet.id == 25) {
      setWhiteBg(prefix + cubelet.id + ' .sticker.yellow');
    }
    if (cubelet.id == 20) {
      setWhiteBg(prefix + cubelet.id + ' .sticker.yellow');
      setWhiteBg(prefix + cubelet.id + ' .sticker.orange');
    }
  });
  */
  // 设置魔方颜色的代码 end
//  alert("a1");
  setTimeout(scrambleCube, 1000); //如果不执行此方法，魔方无法显示
}
//方法 setupLogo end

//与deviceMotion有关 begin
//执行了pauseDeviceMotion()方法后，魔方失去浮动效果
function enableDeviceMotion() {
  if (!motion) {
    motion = deviceMotion(cube, container);
    motion.decay = 0.1;
    motion.range.x = Math.PI * 0.02;
    motion.range.y = Math.PI * 0.02;
    motion.range.z = 0;
  }
  motion.paused = false;
}
function pauseDeviceMotion() {
  motion.paused = true;
}
//与deviceMotion有关 end

/*
//这个是无用函数 begin
function initiateAudio() {
  cube.audioList = [
    'CubeDoodle01',
    'CubeDoodle02',
    'CubeDoodle03',
    'CubeDoodle04',
    'CubeDoodle05',
    'CubeDoodle06',
    'CubeDoodle07',
    'CubeDoodle08'
  ];
  cube.audio = new Html5Audio(cube.audioList,
      'examples/doodle-iframe/media/SingleSounds');
  cube.audio.loadAll();

  cube.addEventListener('onTwistComplete', function(e) {
    cube.audio.play(cube.audioList[
        Math.floor(Math.random() * (cube.audioList.length - 1))]);
  });

}
//这个是无用函数 end
*/

function startInteractiveCube() {
  if (!isMobile) {
    enableDeviceMotion();
  }
  var sentCertificate = false;
  startTime = (new Date()).getTime();
  cube.twistDuration = 500;
  cube.moveCounter = 0;
  cube.addEventListener('onTwistComplete', function(e) {
    if (cube.undoing) {
      cube.moveCounter++;
    }
    if (moveCounter) {
      moveCounter.innerText = moveCounter.textContent = cube.moveCounter;
    }
    postParentMessage({'moves': cube.moveCounter});
	/*
    if (cube.isSolved()) {
      setTimeout(function() {
        cube.hideInvisibleFaces = false;
        cube.showIntroverts();
        if (shadow && shadow.domElement) {
          shadow.domElement.style.opacity = '0';
        }
        if (!sentCertificate) {
          sentCertificate = true;
          doCertificate();
          postParentMessage({'certificate': 1});
        }
      }, 1000);
    }
	*/

  });
  moveCounter.innerText = moveCounter.textContent = cube.moveCounter;
  uipanel.style.opacity = '1';
  if (!isMobile) {
    buttonpanel.style.display = 'none';
    moveCounter.style.display = 'none';
  }
  postParentMessage({'controls': 1});
  cube.mouseControlsEnabled = false;
  cube.keyboardControlsEnabled = true;
}
function checkQueue(callback) {
  if (cube.twistQueue.history.length - cube.twistCountDown == 0) {
    cube.removeEventListener('onTwistComplete', scopedCheckQueue);
    callback();
  }
}
//监听事件 begin
window.addEventListener('message', function(e) {
  var data = e.data;
  if (e.origin != dataStore.origin || data.session != dataStore.session) {
    return;
  }
  var update = data['update'];
  if (update) {
    mergeObject(update, dataStore);
  } else if (data['help']) {
    handleHelpClick();
  } else if (data['moveCounter']) {
    solveCube();
  }
});
//监听事件 end
function postParentMessage(data) {
  data['session'] = dataStore['session'];
  window.parent && window.parent.postMessage(data, dataStore['origin'] || '*');
}
function startScrambleAnimation() {
  postParentMessage({'transition': 1});
  cube.show();//显示魔方
}
function scrambleCube() {
  new TWEEN.Tween(cube.position)
  .to({
        x: 0,
        y: 0,
        z: 0
      }, 3000)
  .easing(TWEEN.Easing.Quartic.InOut)
  .start(cube.time);
  new TWEEN.Tween(cube.rotation)
  .to({
        x: (25).degreesToRadians(),
        y: (45).degreesToRadians(),
        z: 0
      }, 3000)
  .easing(TWEEN.Easing.Quartic.InOut)
  .start(cube.time);
  cube.twistDuration = 120;
//  var WCA_SCRAMBLE_SHORT = 'ddurrdllrBffDUbffdurfdUbll'+'LLBuDFRUDFFBudFFbRLLDRRUDD';
  var WCA_SCRAMBLE_SHORT = 'Bb';
  cube.twistCountDown =
      WCA_SCRAMBLE_SHORT.length + cube.twistQueue.history.length;
//cube.twistCountDown =cube.twistQueue.history.length;
  cube.twist(WCA_SCRAMBLE_SHORT);
  scopedCheckQueue = checkQueue.bind(this, startInteractiveCube);
  cube.addEventListener('onTwistComplete', scopedCheckQueue);
  cube.cubelets.forEach(function(cubelet, indexCubelets) {
    cubelet.faces.forEach(function(face, indexFaces) {
      var sticker = face.element.getElementsByClassName('sticker')[0];
      if (sticker) {
        var colorNow = sticker.style.backgroundColor;
        if (colorNow) {
          colorNow = colorNow.replace(/[^\d,]/g, '').split(',');
          colorNow = {
            r: colorNow[0].toNumber(),
            g: colorNow[1].toNumber(),
            b: colorNow[2].toNumber()
          };
          var colorTarget = _.hexToRgb(face.color.hex);
          new TWEEN.Tween(colorNow)
          .to({
                r: colorTarget.r,
                g: colorTarget.g,
                b: colorTarget.b
              }, 500)
          .onUpdate(function() {
                sticker.style.backgroundColor = 'rgb(' +
                    colorNow.r.round() + ',' +
                    colorNow.g.round() + ',' +
                    colorNow.b.round() + ')';
              })
          .start(cube.time);
        }
      }
    });
  });
}
function solveCube(_twistDuration) {
  _twistDuration = _twistDuration || 0;
  cube.twistDuration = _twistDuration;
  while (cube.twistQueue.history.length > 0){
	  var nextOrder=cube.twistQueue.history[cube.twistQueue.history.length-1];
	  if(nextOrder!="")
	cube.undo();
	  else
	cube.twistQueue.history.pop();
  }	  
}

var helpBubble = document.getElementById('helpbubble');
var helpButton = document.getElementById('helpbutton');
var helpIndex = 0;
var helpNext = document.getElementById('helpnext');
var helpText = document.getElementById('helptext');
var helpImage = document.getElementById('helpimage');
var moveCounter = document.getElementById('movecounter');
var searchButton = document.getElementById('searchbutton');
var shareBubble = document.getElementById('sharebubble');
var shareButton = document.getElementById('sharebutton');
var shareEmail = document.getElementById('shareemail');
var shareFacebook = document.getElementById('sharefacebook');
var shareGPlus = document.getElementById('sharegplus');
var shareShortLink = document.getElementById('shareshortlink');
var shareTwitter = document.getElementById('sharetwitter');
var uipanel = document.getElementById('uipanel');
var buttonpanel = document.getElementById('buttonpanel');
if (window.navigator.userAgent.match(/iP(hone|od|ad)/i)) {
  shareEmail.style.display = 'none';
}
function setMobileIcon(element) {
  element.className = element.className.replace('_64', '_96');
}
if (isMobile) {
  setMobileIcon(shareGPlus);
  setMobileIcon(shareFacebook);
  setMobileIcon(shareTwitter);
  setMobileIcon(shareEmail);
  setMobileIcon(searchButton);
  setMobileIcon(helpButton);
  setMobileIcon(shareButton);
  shareShortLink.style.display = 'none';
}
function addListener(element, listener) {
  if (isMobile) {
    element.addEventListener('touchstart', listener);
  } else {
    element.addEventListener('click', listener);
  }
}
/*
function getShortlinkForSharing() {
  var shortlink = dataStore['shortlink'] || '//google.com/doodles';
  return shortlink.replace(/.*\/\//, 'http://');
}

//谷歌页面的按钮功能 begin
addListener(shareGPlus, function(e) {
  window.open('https://plus.google.com/share?url=' +
      encodeURIComponent(getShortlinkForSharing()));
});
addListener(shareFacebook, function(e) {
  window.open('http://www.facebook.com/sharer.php?u=' +
      encodeURIComponent(getShortlinkForSharing()));
});
addListener(shareTwitter, function(e) {
  window.open('http://twitter.com/intent/tweet?status=' +
      encodeURIComponent(dataStore['msgs']['Share Message'] + ' ' +
      getShortlinkForSharing()));
});
addListener(shareEmail, function(e) {
  window.open('mailto:?subject=' +
      encodeURIComponent(dataStore['msgs']['Share Message']) +
      '&body=' + encodeURIComponent(getShortlinkForSharing()));
});
addListener(searchButton, function(e) {
  postParentMessage({'search': 1});
});
//谷歌页面的按钮功能 end
*/
/*
function updateHelp() {
  helpText.textContent = helpText.innerText =
      dataStore['msgs']['Directions ' + helpIndex];
  helpNext.textContent = helpNext.innerText =
      dataStore['msgs']['Directions UI ' + (helpIndex < 2 ? 1 : 2)];
  if (helpIndex == 1) {
    helpImage.classList.remove('two');
    helpImage.classList.add('one')
  } else if (helpIndex == 2) {
    helpImage.classList.remove('one');
    helpImage.classList.add('two')
  }
}

function handleHelpClick(e) {
  if (helpBubble.style.display == 'none') {
    helpBubble.style.display = 'block';
    helpBubble.style.pointerEvents = 'auto';
    helpIndex = 1;
    updateHelp();
  } else {
    helpBubble.style.display = 'none';
    helpBubble.style.pointerEvents = 'none';
  }
  shareBubble.style.opacity = '0';
}

helpBubble.dir = dataStore['dir'];
addListener(helpBubble, function(e) {
  helpIndex++;
  if (helpIndex > 2) {
    helpBubble.style.display = 'none'
    helpBubble.style.pointerEvents = 'none';
  } else {
    helpBubble.style.display = 'block'
    updateHelp();
  }
  shareBubble.style.opacity = 'none';
  e.preventDefault && e.preventDefault();
  return false;
});
addListener(helpButton, handleHelpClick);
addListener(container, function(e) {
  helpBubble.style.display = 'none';
  helpBubble.style.pointerEvents = 'none';
  shareBubble.style.opacity = '0';
});
addListener(shareButton, function(e) {
  shareBubble.style.opacity = shareBubble.style.opacity == '0' ? '1' : '0';
  helpBubble.style.display = 'none';
  shareShortLink.value = dataStore['shortlink'].replace(/^\/\//, '');
});
shareShortLink.addEventListener('mouseup', function(e) {
  e.preventDefault();
  shareShortLink.select();
});
*/
function afterViewCube(orderOfCubeReserve){
//	var cubeWin=document.getElementById('cubeFrame').contentWindow;
	solveCube();
	startInteractiveCube();
	
	var orders=orderOfCubeReserve.split(',')
	for(var i1=0;i1<orders.length;i1++){
	if(orders[i1]!="")
		cube.twist(orders[i1]);
	}
}



startInteractiveCube();
cube.undoing=true;
//cube.twist('l');
//solveCube();
//cube.twistQueue.history.length
//solveCube();
//startInteractiveCube();
//cube.twistQueue.future=new Array();
//cube.twist('u');
var command="";
function getCommand(){
	var s="";
	for(var i1=0;i1<cube.twistQueue.history.length;i1++){
		if(cube.twistQueue.history[i1]!='')
		s+=cube.twistQueue.history[i1].command+',';
	}
	s=s.substring(0, s.length-1);
	return s;
}

function beforeView(){
	command=getCommand();
}

function afterView(){
	resetCube();
	setTimeout("cube.twist(command);",500);
	setTimeout('cube.twistDuration=500;',3000);
}

function resetCube(){
	cube.twistDuration=0;
	while(cube.twistQueue.history.length>0){
		if(cube.twistQueue.history[cube.twistQueue.history.length-1]!=""){
			cube.undo();
		}else{
			cube.twistQueue.history.pop();
		}
	}	
}



//cube.twistDuration=0;cube.undo();cube.undo();cube.undo();setTimeout("cube.twistQueue.empty();cube.twist('bBl')",1000);